package com.lindosoft.android.common;

import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/** Swipe gestures. */
public class Swipe {
  public static int swipeThreshold = 100;

  /** Register swipe motion. */
  public static void registerSwipe(boolean onDown, SwipeListener swipeListener, View view) {
    final GestureDetector gestureDetector = new GestureDetector(new SwipeOnGestureListener(onDown, swipeListener));
    view.setOnTouchListener(new OnTouchListener() {
      public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
      }
    }); 
  }

  /** Listen for swipe gestures. */
  private static class SwipeOnGestureListener extends SimpleOnGestureListener {
    private SwipeListener swipeListener = null;
    private boolean onDown = false;

    public SwipeOnGestureListener(boolean onDown, SwipeListener swipeListener) {
      this.onDown = onDown;
      this.swipeListener = swipeListener;
    }

    @Override
    public boolean onDown(MotionEvent e) {
      return onDown;
    }

    @Override
    public boolean onSingleTapConfirmed (MotionEvent e) {
      swipeListener.onTap();
      return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
      float diffX = e2.getX() - e1.getX();
      float diffY = e2.getY() - e1.getY();
      if (Math.abs(diffX) < Math.abs(diffY)) {
        // no horizontal swipe, vertical scroll
      } else if (diffX < -swipeThreshold && Math.abs(velocityX) > swipeThreshold / 2) {
        swipeListener.onForward();
        return true;
      } else if (diffX > swipeThreshold && Math.abs(velocityX) > swipeThreshold / 2) {
        swipeListener.onBack();
        return true;
      }
      return false;
    }
  }
}