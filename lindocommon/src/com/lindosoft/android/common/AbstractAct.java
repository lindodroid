package com.lindosoft.android.common;

import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * Common base of list activities.
 * Insert <ListView android:id="@android:id/list"/> in layout. 
 */
public abstract class AbstractAct extends ListActivity implements SwipeListener {
  protected int screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

  @Override
  public void onBack() {  
    new Thread() {
      public void run() {
        try {
          doBack();
          Thread.sleep(500L);
          finish();
        } catch (Throwable ignore) {
          Logger.warn(getClass(), "doBack()", ignore);
        }
      }
    }.start();
  }
  
  @Override
  public void onBackPressed() {
    onBack();
  }

  @Override
  public void onForward() {
    try {
      doForward();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doForward()", ignore);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBack();
        return true;
      }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onTap() {
    try {
      doTap();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doTap()", ignore);
    }
  }

  /** Called from onBack(). */
  protected void doBack() throws Exception {
  }

  /** Called from onCreate(). */
  protected void doCreate(Bundle bundle) throws Exception {
  }

  /** Called from onDestroy(). */
  protected void doDestroy() throws Exception {
  }

  /** Called from onForward(). */
  protected void doForward() throws Exception {
  }

  /** Called from onPause(). */
  protected void doPause() throws Exception {
  }

  /** Called from onRestoreInstanceState(). */
  protected void doRestore(Bundle bundle) throws Exception {
  }

  /** Called from onResume(). */
  protected void doResume() throws Exception {
  }

  /** Called from onSaveInstanceState(). */
  protected void doSave(Bundle bundle) throws Exception {
  }

  /** Called from onStart(). */
  protected void doStart() throws Exception {
  }

  /** Called from onStop(). */
  protected void doStop() throws Exception {
  }

  /** Called from onTap(). */
  protected void doTap() throws Exception {
  }

  @Override
  protected void onCreate(Bundle bundle) {
    try {
      super.onCreate(bundle);
      getActionBar().setDisplayHomeAsUpEnabled(true);
      //setRequestedOrientation(screenOrientation);
      doCreate(bundle);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doCreate()", ignore);  
    }
  }

  @Override
  protected void onDestroy() {
    try {
      doDestroy();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doDestroy()", ignore);  
    }
    try {
      super.onDestroy();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "onDestroy()", ignore);  
    }
  }

  @Override
  protected void onPause() {
    try {
      doPause();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doPause()", ignore);  
    }
    try {
      super.onPause();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "onPause()", ignore);  
    }
  }

  @Override
  protected void onRestoreInstanceState(Bundle bundle) {
    try {
      super.onRestoreInstanceState(bundle);
      doRestore(bundle);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doRestore()", ignore);  
    }
  }

  @Override
  protected void onResume() {
    try {
      super.onResume();
      doResume();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doResume()", ignore);  
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle bundle) {
    try {
      super.onSaveInstanceState(bundle);
      doSave(bundle);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doSave()", ignore);  
    }
  }

  @Override
  protected void onStart() {
    try {
      super.onStart();
      doStart();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doStart()", ignore);  
    }
  }

  @Override
  protected void onStop() {
    try {
      doStop();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "doStop()", ignore);  
    }
    try {
      super.onStop();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "onStop()", ignore);  
    }
  }

  /** Rotate screen clockwise. */
  protected void rotateScreen() {
    switch(screenOrientation) {
      case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        break;
      case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
        break;
      case ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE:
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        break;
      default:
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
    setRequestedOrientation(screenOrientation);
  }

  /** Scroll to the selected position. */
  protected void scroll(final int position) {
    getListView().postInvalidate();
    getListView().post(new Runnable() {            
      @Override
      public void run() {
        try {
          getListView().setSelection(position);
        } catch (Throwable ignore) {
          Logger.warn(getClass(), "scroll()", ignore);  
        }
      }
    });
  }
}