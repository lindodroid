package com.lindosoft.android.common;

/** Listen for swipe gestures. */
public interface SwipeListener {
  /** Left to right swipe. */
  void onBack();
  
  /** Right to left swipe. */
  void onForward();

  /** Single tap. */
  void onTap();
}