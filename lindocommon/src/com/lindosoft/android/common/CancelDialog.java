package com.lindosoft.android.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;

/** Provide time boxed ability to cancel action. */
public class CancelDialog {
  /* Display progress dialog for timeout ms and do the things defined in doRun() if not cancelled. */ 
  public void show(final Activity activity, final String error, final String prompt, final long timeout) {
    final ProgressDialog progress = ProgressDialog.show(activity, "", prompt, true, true);
    new Thread() {
      public void run() {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeout) {
          try {
            Thread.sleep(timeout / 10L);
          } catch (Exception ignore) {
          }
        }
        activity.runOnUiThread(new Runnable() {
          public void run() {
            try {
              if (progress.isShowing()) {
                progress.cancel();
                doRun();
              }
            } catch (Throwable ex) {
              Toast.makeText(activity, error + ex, Toast.LENGTH_LONG).show();
            }
          }
        });
      }
    }.start();
  }

  /** If not cancelled these things will run on the uiThread. */
  protected void doRun() throws Exception {
  }
}