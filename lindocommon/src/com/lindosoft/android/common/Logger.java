package com.lindosoft.android.common;

import android.util.Log;
import java.io.*;
import java.text.*;
import java.util.*;

/** File logger. */
public class Logger {
	public static final String EXTENSION = ".txt";
	public static final String SEPARATOR = "/";
	private static File logfile = null;
	
	public static void init(File home) {
		logfile = null;
		try {
			home.mkdirs();
			logfile = new File(home, new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + EXTENSION);
		} catch (Exception ignore) {
		}
	}
	
	public static void debug(Class type, Object message) {
    debug(type, message, null);
	}

	public static void debug(Class type, Object message, Throwable throwable) {
    Log.d(type.getSimpleName(), "" + message, throwable);
		log(type, "D", message, throwable);
	}


	public static void error(Class type, Object message) {
    error(type, message, null);
	}

	public static void error(Class type, Object message, Throwable throwable) {
    Log.e(type.getSimpleName(), "" + message, throwable);
		log(type, "E", message, throwable);
	}


	public static void info(Class type, Object message) {
    info(type, message, null);
	}

	public static void info(Class type, Object message, Throwable throwable) {
    Log.i(type.getSimpleName(), "" + message, throwable);
		log(type, "I", message, throwable);
	}


	public static void warn(Class type, Object message) {
    warn(type, message, null);
	}

	public static void warn(Class type, Object message, Throwable throwable) {
    Log.w(type.getSimpleName(), "" + message, throwable);
		log(type, "W", message, throwable);
	}
	

	public static void log(Class type, String level, Object message, Throwable throwable) {
		if (null == logfile) {
			return;
		}
		PrintWriter printer = null;
		try {
			printer = new PrintWriter(new FileWriter(logfile, true));
			printer.println(new StringBuilder().append(new SimpleDateFormat("HH:mm:ss").format(new Date()))
				.append(SEPARATOR).append(level).append(SEPARATOR).append(type.getSimpleName()).append(SEPARATOR).append("" + message)
				.toString());
			if (null != throwable) {
				throwable.printStackTrace(printer);
			}
		} catch (Exception ignore) {
		} finally {
			try {
				printer.close();
			} catch (Exception ignore) {
			}
		}
	}	
}
