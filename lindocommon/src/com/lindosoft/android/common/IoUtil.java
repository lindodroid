package com.lindosoft.android.common;

import java.io.*;

/** Utils for java.io package. */
public class IoUtil {
  /** The buffer size in bytes for copy operations. */
  public static final int COPY_BUFFER_SIZE = 1024;

  /**
   * Close the Closeable.
   * @param closeable The instance on which the close() method shall be called.
   * @return When close() method could be called without exception,	return true.
   */
  public static boolean close(final Closeable closeable) {
    try {
      closeable.close();
    }
    catch (Exception ex) {
      return false;
    }
    return true;
  }

  /** Copy in to out. */
  public static void copyFile(final File in, final File out) throws IOException {
    if (null == in || null == out) {
      return;
    }
    FileInputStream input = new FileInputStream(in);
    FileOutputStream output = new FileOutputStream(out);
    try {
      copyStream(input, output);
    }
    finally {
      close(input);
      close(output);
    }
  }

  /**
   * Copy from in to out using byte buffer of size COPY_BUFFER_SIZE.
   * @param in The InputStream.
   * @param out The OutputStream.
   * @throws IOException Could not copy from in to out.
   */
  public static void copyStream(final InputStream in, final OutputStream out) throws IOException {
    if (null == in || null == out) {
      return;
    }
    byte[] buffer = new byte[COPY_BUFFER_SIZE];
    int length = -1;
    final int offset = 0;
    while (-1 != (length = in.read(buffer, offset, COPY_BUFFER_SIZE))) {
      out.write(buffer, offset, length);
    }
    out.flush();
  }
}
