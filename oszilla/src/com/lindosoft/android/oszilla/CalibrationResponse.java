package com.lindosoft.android.oszilla;

import com.lindosoft.android.common.*;
import java.nio.ByteBuffer;

public class CalibrationResponse {
  int[][] sampleOffset = new int[7][4]; // groundSample
  int[][] sampleRange = new int[7][4]; // highSample - lowSample
  float[][] voltRange = new float[7][4]; // highVolt - lowVolt 

  public void setTelegram(int size, byte[] telegram) {
    int pos = 4; // start byte / length / length / cmdId
    for (int gain = 0; gain < 7; gain++) {
      for (int point = 0; point < 3; point++) {
        byte[] voltBuffer = new byte[] {telegram[pos + 3], telegram[pos + 2], telegram[pos + 1], telegram[pos]}; 
        float volt = ByteBuffer.wrap(voltBuffer).getFloat();
        pos += 4;
        for (int channel = 0; channel < 4; channel++) {
          int sample = (short) ((0xff & telegram[pos]) | (telegram[pos + 1] << 8));
          // Logger.info(getClass(), "gain=" + gain + ", point=" + point + ", channel=" + channel);
          // Logger.info(getClass(), "volt=" + volt + ", sample=" + sample);
          if (0 == point) {
            sampleOffset[gain][channel] = sample;
          } else if (1 == point) {
            sampleRange[gain][channel] = sample;
            voltRange[gain][channel] = volt;
          } else if (2 == point) {
            sampleRange[gain][channel] -= sample;
            voltRange[gain][channel] -= volt;
          }
          pos += 2;
        }
      }
    }    
  }

  /** Get offset by channel (1 - 4) and gain (0 - 6). */
  public int getOffset(int channel, int gain) {
    return sampleOffset[gain][channel - 1];
  }

  /** Get factor (volts per unit) by channel (1 - 4) and gain (0 - 6). */
  public float getFactor(int channel, int gain) {
    return 0 == sampleRange[gain][channel - 1] ? 0f : voltRange[gain][channel - 1] / (float) sampleRange[gain][channel - 1];
  }
}