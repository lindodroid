package com.lindosoft.android.oszilla;

import java.util.ArrayList;
import java.util.List;

public class ByteBuffer {
  private List<Byte> bytes;

  public ByteBuffer(int length) {
    bytes = new ArrayList(length * 2); // worst case
    bytes.add((byte) 0xff);
    bytes.add((byte) length);
  }

  public void add(int value) {
    bytes.add((byte) value);
    if (0xff == value) { // escape start byte
      bytes.add((byte) value);
    }
  }

  public byte[] getBytes() {
    byte[] byteArray = new byte[bytes.size()];
    for (int i = 0; i < bytes.size(); i++) {
      byteArray[i] = bytes.get(i);
    }
    return byteArray;
  }
}