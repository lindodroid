package com.lindosoft.android.oszilla;

public class CalibrationRequest {
  public byte[] getBytes() {
    ByteBuffer buffer = new ByteBuffer(8);
    buffer.add(0x02); // cmdId
    buffer.add(0x00); // type
    buffer.add(0x00); // volt[1]
    buffer.add(0x00); // volt[2]
    buffer.add(0x00); // volt[3]
    buffer.add(0x00); // volt[4]
    buffer.add(0x00); // gain
    buffer.add(0x00); // point
    return buffer.getBytes();
  }
}