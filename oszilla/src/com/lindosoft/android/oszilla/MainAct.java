package com.lindosoft.android.oszilla;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Notification.Builder;
import android.view.View;
import android.widget.VideoView;
import com.lindosoft.android.common.*;

/** Main activity. */
public class MainAct extends AbstractAct implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
  private MainModel model = null;
  private String video = "intro";

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    if ("intro".equals(video)) {
      startOszi();
    } else {
      onBack();
    }
  }

  @Override
  public boolean onError(MediaPlayer mp, int what, int extra) {
    if ("intro".equals(video)) {
      startOszi();
    } else {
      onBack();
    }
    return true;
  }

  /** Oszi clicked. */
  public void onOszi(View view) {
    startOszi();
  }


  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.main);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_main));
    model = new MainModel(this);
    model.doCreate(bundle);
    video = bundle.getString("video", "intro");
  }

  @Override
  protected void doDestroy() {
    removeNotification();
  }

  @Override
  protected void doForward() {
    startOszi();
  }
  
  @Override
  protected void doPause() {
    stopVideo();
  }
  
  @Override
  protected void doResume() {
    addNotification();
    startVideo();
  }

  @Override
  protected void doSave(Bundle bundle) {
    bundle.putString("video", video);
  }


  /** Add/update notification. */
  private void addNotification() {
    Intent intent = new Intent(this, this.getClass());
    intent.setAction(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_LAUNCHER);
    Builder builder = new Builder(this)
        .setContentText(getString(R.string.content_text))
        .setContentTitle(getString(R.string.label))
        .setSmallIcon(R.drawable.icon);
    builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    Notification notification = builder.build();
    notificationManager.notify(1, notification);
  }

  /** Remove notification. */
  private void removeNotification() {
    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancelAll();
  }

  /** Start ozzi activity. */
  private void startOszi() {
    video = "outro";
    Intent intent = new Intent(this, OsziAct.class);
    startActivity(intent);
  }

  /** Start video. */
  private void startVideo() {
    stopVideo();
    final VideoView videoView = (VideoView) findViewById(R.id.video);
    videoView.setOnCompletionListener(this);
    videoView.setOnErrorListener(this);
    runOnUiThread(new Runnable() {
      public void run() {
        try {
          Thread.sleep(500);
          int identifier = getResources().getIdentifier(video, "raw", getPackageName());
          Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + identifier); 
          videoView.setVideoURI(uri);
          videoView.setVisibility(View.VISIBLE);
          videoView.start();
        } catch (Exception ex) {
          Logger.warn(getClass(), "startVideo(" + video + ") failed", ex);
          stopVideo();
        }
      }
    });
  }

  /** Stop video. */
  private void stopVideo() {
    try {
      VideoView videoView = (VideoView) findViewById(R.id.video);
      videoView.setVisibility(View.INVISIBLE);
      videoView.stopPlayback();
    } catch (Exception ex) {
      Logger.warn(getClass(), "stopVideo() failed", ex);
    }
  }
}