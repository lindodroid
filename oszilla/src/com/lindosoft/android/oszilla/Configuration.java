package com.lindosoft.android.oszilla;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Configuration {
  int ch1Gain = 1; // 0 - 6 (1x - 64x)
  int ch2Gain = 1;
  int ch3Gain = 1;
  int ch4Gain = 1;
  boolean ch1On = true;
  boolean ch2On = false;
  boolean ch3On = false;
  boolean ch4On = false;
  String devicePrefix = "M0006";
  int testHigh = 50; // 1 - 2000000
  int testPeriod = 200; // 1 - 2000000
  int time = 5; // 1 - 16 (10Ms - 1s)
  int trLevel = 512; // 0 - 4095
  int trChannel = 1; // 1 - 4
  boolean trAuto = true;
  boolean trDown = false;

  public byte[] getBytes() {
    ByteBuffer buffer = new ByteBuffer(15);
    buffer.add(0x01); // cmdId
    buffer.add(time); // time
    buffer.add((ch4On ? 8 : 0) + (ch3On ? 4 : 0) + (ch2On ? 2 : 0) + (ch1On ? 1 : 0)); // ch4On/ch3On/ch2On/ch1On
    buffer.add(16 * ch2Gain + ch1Gain); // ch2Gain/ch1Gain
    buffer.add(16 * ch4Gain + ch3Gain); // ch4Gain/ch3Gain
    buffer.add(trLevel); // trLevel[1]
    buffer.add((trDown ? 128 : 0) + (trAuto ? 64 : 0) + (16 * trChannel - 16) + (trLevel  / 256)); // trDown/trAuto/trChannel/trLevel[2]
    buffer.add(testPeriod); // testPeriod[1]
    buffer.add(testPeriod / 256); // testPeriod[2]
    buffer.add(testPeriod / 65536); // testPeriod[3]
    buffer.add(0x00); // testPeriod[4]
    buffer.add(testHigh); // testHigh[1]
    buffer.add(testHigh / 256); // testHigh[2]
    buffer.add(testHigh / 65536); // testHigh[3]
    buffer.add(0x00); // testHigh[4]
    return buffer.getBytes();
  }

  public void read(SharedPreferences preferences) {
    if (null == preferences) return;
    ch1Gain = preferences.getInt("ch1Gain", 1);
    ch2Gain = preferences.getInt("ch2Gain", 1);
    ch3Gain = preferences.getInt("ch3Gain", 1);
    ch4Gain = preferences.getInt("ch4Gain", 1);
    ch1On = preferences.getBoolean("ch1On", true);
    ch2On = preferences.getBoolean("ch2On", false);
    ch3On = preferences.getBoolean("ch3On", false);
    ch4On = preferences.getBoolean("ch4On", false);
    devicePrefix = preferences.getString("devicePrefix", "M0006");
    testHigh = preferences.getInt("testHigh", 50);
    testPeriod = preferences.getInt("testPeriod", 200);
    time = preferences.getInt("time", 5);
    trLevel = preferences.getInt("trLevel", 512);
    trChannel = preferences.getInt("trChannel", 1);
    trAuto = preferences.getBoolean("trAuto", true);
    trDown = preferences.getBoolean("trDown", false);
  }

  public void write(SharedPreferences preferences) {
    if (null == preferences) return;
    Editor editor = preferences.edit();
    editor.putInt("ch1Gain", ch1Gain);
    editor.putInt("ch2Gain", ch2Gain);
    editor.putInt("ch3Gain", ch3Gain);
    editor.putInt("ch4Gain", ch4Gain);
    editor.putBoolean("ch1On", ch1On);
    editor.putBoolean("ch2On", ch2On);
    editor.putBoolean("ch3On", ch3On);
    editor.putBoolean("ch4On", ch4On);
    editor.putString("devicePrefix", devicePrefix);
    editor.putInt("testHigh", testHigh);
    editor.putInt("testPeriod", testPeriod);
    editor.putInt("time", time);
    editor.putInt("trLevel", trLevel);
    editor.putInt("trChannel", trChannel);
    editor.putBoolean("trAuto", trAuto);
    editor.putBoolean("trDown", trDown);
    editor.apply();
  }
}