package com.lindosoft.android.oszilla;

import android.content.SharedPreferences;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.support.v4.content.LocalBroadcastManager;
import com.bmxgates.logger.BluetoothSerial;
import com.lindosoft.android.common.*;

/** Oszi model. */
public class OsziModel {
  OsziAct act;
  CalibrationResponse calibration = new CalibrationResponse();
  Configuration configuration = new Configuration();
  int connectionIcon;
  int connectionTitle;
  int size = 0;
  byte[] telegram = new byte[4096];
  long total = 0L;
  private BluetoothSerial bluetoothSerial;
  private BroadcastReceiver broadcastReceiver;
  private boolean ffOld = false;
  
  public OsziModel(OsziAct act) {
    this.act = act;
  }

  public void readConfiguration() {
    try {
      configuration.read(PreferenceManager.getDefaultSharedPreferences(act));
      act.updateConfigurationView();
    } catch (Exception ignore) {
      Logger.warn(getClass(), "writeConfiguration() failed", ignore);
    }
  }

  public void writeConfiguration() {
    try {
      if (null != bluetoothSerial) {
        bluetoothSerial.setDevicePrefix(configuration.devicePrefix);
      }
      configuration.write(PreferenceManager.getDefaultSharedPreferences(act));
      bluetoothSerial.write(configuration.getBytes());
    } catch (Exception ignore) {
      Logger.warn(getClass(), "writeConfiguration() failed", ignore);
    }
  }

  public void doCreate(Bundle bundle) {
    readConfiguration();
		// callback when bytes are read from bluetooth serial
		bluetoothSerial = new BluetoothSerial(act, new BluetoothSerial.MessageHandler() {
			@Override
			public int read(int bufferSize, byte[] buffer) {
				return doRead(bufferSize, buffer);
			}
		}, configuration.devicePrefix);
  }

  public void doDestroy() {
    bluetoothSerial.close();
  }

  public void doPause() {
    bluetoothSerial.onPause();
    unregisterReceiver();
  }

  public void doResume() {
    ffOld = false;
    total = 0L;
    registerReceiver();
    bluetoothSerial.onResume();
  }


  /** Bluetooth read callback. */
  private int doRead(int bufferSize, byte[] buffer) {
      for (int i = 0; i < bufferSize; i++) {
        boolean ffNew = 255 == (0xff & buffer[i]);
        if (ffOld && !ffNew) {
          if (size > 0) {
            // stop
            handleTelegram();
            size = 0;
          }
          // start
          telegram[size++] = (byte) 0xff;
          telegram[size++] = buffer[i];
        } else if (ffOld == ffNew && size > 0) {
          // append
          telegram[size++] = buffer[i];
        }
        ffOld = ffNew && !ffOld;
      }
      return bufferSize;
  }

  private void handleTelegram() {
    total++;
    int cmdId = 0xff & telegram[3]; 
    // Logger.info(getClass(), "cmdId(" + cmdId + ")");
    if (1 == cmdId) {
      handleMeasurement();
    } else if (2 == cmdId) {
      handleCalibration();
    }
  }

  private void handleCalibration() {
    // Logger.info(getClass(), "handleCalibration(" + size + ")");
    calibration.setTelegram(size, telegram);
    act.updateMainView();
  }

  private void handleMeasurement() {
    // Logger.info(getClass(), "handleMeasurement(" + size + ")");
    act.updateImageView();
  }

  /** Bluetooth connected. */
  private void onConnected() {
    // Logger.info(getClass(), "onConnected()");
    readConfiguration();
    writeConfiguration();
    try {
      bluetoothSerial.write(new CalibrationRequest().getBytes());
    } catch (Exception ignore) {}
    connectionIcon = R.drawable.bt_connected;
    connectionTitle = R.string.bt_connected;
    act.updateMainView();
  }

  /** Bluetooth disconnected. */
  private void onDisconnected() {
    // Logger.info(getClass(), "onDisconnected()");
    connectionIcon = R.drawable.bt_disconnected;
    connectionTitle = R.string.bt_disconnected;
    act.updateMainView();
  }

  /** Bluetooth connection failed. */
  private void onFailed() {
    // Logger.info(getClass(), "onFailed()");
    connectionIcon = R.drawable.bt_failed;
    connectionTitle = R.string.bt_failed;
    act.updateMainView();
  }

  /** Register local broadcast receiver for bluetooth events. */
  private void registerReceiver() {
    broadcastReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        // Logger.info(getClass(), "onReceive(" + intent + ")");
        String action = intent.getAction();
        if (BluetoothSerial.BLUETOOTH_CONNECTED.equals(action)) {
          onConnected();
        } else if (BluetoothSerial.BLUETOOTH_DISCONNECTED.equals(action)) {
          onDisconnected();
        } else if (BluetoothSerial.BLUETOOTH_FAILED.equals(action)) {
          onFailed();
        }
      }
    };
    LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(act);
    // Fired when connection is established and when onResume is called if a connection is already established. 
    broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(BluetoothSerial.BLUETOOTH_CONNECTED));
    // Fired when the connection is lost
    broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(BluetoothSerial.BLUETOOTH_DISCONNECTED));
    // Fired when connection can not be established, after 30 attempts.
    broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(BluetoothSerial.BLUETOOTH_FAILED));
  }

  /** Unregister local broadcast receiver for bluetooth events. */
  private void unregisterReceiver() {
    LocalBroadcastManager.getInstance(act).unregisterReceiver(broadcastReceiver);
  }
}
