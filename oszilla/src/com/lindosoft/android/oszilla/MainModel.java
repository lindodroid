package com.lindosoft.android.oszilla;

import android.os.Bundle;
import android.os.Environment;
import com.lindosoft.android.common.*;
import java.io.File;

/** Main model. */
public class MainModel {
  public static final String HOME = "oszilla";
  MainAct act;

  public MainModel(MainAct act) {
    this.act = act;
  }

  public void doCreate(Bundle bundle) {
    File home = new File(Environment.getExternalStorageDirectory(), HOME);
    Logger.init(home);
  }
}
