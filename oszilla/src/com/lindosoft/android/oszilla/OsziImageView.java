package com.lindosoft.android.oszilla;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.widget.ImageView;
import android.widget.TextView;
import com.lindosoft.android.common.*;

public class OsziImageView {
  private int[] colors = new int[] {0xa0f08000, 0xa000f0f0, 0xa0f000f0, 0xa0f0f000};
  private OsziModel model;

  public OsziImageView(OsziModel model) {
    this.model = model;
  }

  public void updateView() {
    final Bitmap bitmap = getBitmap();
    model.act.runOnUiThread(new Runnable() {
      public void run() {
        ImageView imageView = (ImageView) model.act.findViewById(R.id.image);
        imageView.setImageBitmap(bitmap);
        imageView.postInvalidate();
        TextView textTotal = (TextView) model.act.findViewById(R.id.text_total);
        textTotal.setText(model.total + "");
      }
    });
  }


  private Bitmap getBitmap() {
    ImageView imageView = (ImageView) model.act.findViewById(R.id.image);
    int height = imageView.getHeight();
    int width = imageView.getWidth();
    if (0 == height || 0 == width) {
      return null;
    }
    final int min = Math.min(height, width);
    Bitmap bitmap = Bitmap.createBitmap(min, min, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    //canvas.drawRGB(0, 0, 0);
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    paint.setStrokeCap(Paint.Cap.ROUND);
    paint.setStrokeJoin(Paint.Join.ROUND);
    paint.setStrokeWidth(8);
    paint.setStyle(Paint.Style.STROKE);
    for (int i = 1; i < 5; i++) {
      paint.setColor(colors[i - 1]);
      final int offset = model.calibration.getOffset(i, model.configuration.ch1Gain);
      final Path path = new Path();
      Measurement.accept(model.size, model.telegram, i, new SampleVisitor() {
        PointF previousPointF = null;
        public void visit(float pos, float sample) {
          PointF pointF = new PointF(min * pos / 1000f, min / 2f - (sample - (float) offset));
          if (null != previousPointF) {
            path.moveTo(previousPointF.x, previousPointF.y);
            path.lineTo(pointF.x, pointF.y);
          }
          previousPointF = pointF;
        }
      });
      canvas.drawPath(path, paint);
    }
    return bitmap;
  }
}