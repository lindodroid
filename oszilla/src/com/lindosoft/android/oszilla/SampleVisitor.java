package com.lindosoft.android.oszilla;

/** Visit single measurement sample. */
public interface SampleVisitor {
  void visit(float pos, float sample);
}