package com.lindosoft.android.oszilla;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;

public class OsziConfigurationView {
  private OsziModel model;

  public OsziConfigurationView(OsziModel model) {
    this.model = model;
    // numberPicker
    initNumberPicker(R.id.number_picker_test_high, 2000000, 1);
    initNumberPicker(R.id.number_picker_test_period, 2000000, 1);
    initNumberPicker(R.id.number_picker_tr_level, 4095, 1);
    // spinner
    initSpinner(R.id.spinner_ch1_gain, R.array.array_ch_gain);
    initSpinner(R.id.spinner_ch2_gain, R.array.array_ch_gain);
    initSpinner(R.id.spinner_ch3_gain, R.array.array_ch_gain);
    initSpinner(R.id.spinner_ch4_gain, R.array.array_ch_gain);
    initSpinner(R.id.spinner_time, R.array.array_time);
    initSpinner(R.id.spinner_tr_channel, R.array.array_tr_channel);
  }

  public void updateModel() {
    // editText
    model.configuration.devicePrefix = ((EditText) model.act.findViewById(R.id.edit_text_device_prefix)).getText().toString();
    // numberPicker
    model.configuration.testHigh = getNumberPickerValue(R.id.number_picker_test_high);
    model.configuration.testPeriod = getNumberPickerValue(R.id.number_picker_test_period);
    model.configuration.trLevel = getNumberPickerValue(R.id.number_picker_tr_level);
    // spinner
    model.configuration.ch1Gain = getSpinnerPosition(R.id.spinner_ch1_gain);
    model.configuration.ch2Gain = getSpinnerPosition(R.id.spinner_ch2_gain);
    model.configuration.ch3Gain = getSpinnerPosition(R.id.spinner_ch3_gain);
    model.configuration.ch4Gain = getSpinnerPosition(R.id.spinner_ch4_gain);
    model.configuration.time = getSpinnerPosition(R.id.spinner_time) + 1;
    model.configuration.trChannel = getSpinnerPosition(R.id.spinner_tr_channel) + 1;
    // switch
    model.configuration.ch1On = isSwitchChecked(R.id.switch_ch1_on);
    model.configuration.ch2On = isSwitchChecked(R.id.switch_ch2_on);
    model.configuration.ch3On = isSwitchChecked(R.id.switch_ch3_on);
    model.configuration.ch4On = isSwitchChecked(R.id.switch_ch4_on);
    model.configuration.trAuto = isSwitchChecked(R.id.switch_tr_auto);
    model.configuration.trDown = isSwitchChecked(R.id.switch_tr_down);
  }

  public void updateView() {
    model.act.runOnUiThread(new Runnable() {
      public void run() {
        // editText
        ((EditText) model.act.findViewById(R.id.edit_text_device_prefix)).setText(model.configuration.devicePrefix);
        // numberPicker
        setNumberPickerValue(R.id.number_picker_test_high, model.configuration.testHigh);
        setNumberPickerValue(R.id.number_picker_test_period, model.configuration.testPeriod);
        setNumberPickerValue(R.id.number_picker_tr_level, model.configuration.trLevel);
        // spinner
        setSpinnerPosition(R.id.spinner_ch1_gain, model.configuration.ch1Gain);
        setSpinnerPosition(R.id.spinner_ch2_gain, model.configuration.ch2Gain);
        setSpinnerPosition(R.id.spinner_ch3_gain, model.configuration.ch3Gain);
        setSpinnerPosition(R.id.spinner_ch4_gain, model.configuration.ch4Gain);
        setSpinnerPosition(R.id.spinner_time, model.configuration.time - 1);
        setSpinnerPosition(R.id.spinner_tr_channel, model.configuration.trChannel - 1);
        // switch
        setSwitchChecked(model.configuration.ch1On, R.id.switch_ch1_on);
        setSwitchChecked(model.configuration.ch2On, R.id.switch_ch2_on);
        setSwitchChecked(model.configuration.ch3On, R.id.switch_ch3_on);
        setSwitchChecked(model.configuration.ch4On, R.id.switch_ch4_on);
        setSwitchChecked(model.configuration.trAuto, R.id.switch_tr_auto);
        setSwitchChecked(model.configuration.trDown, R.id.switch_tr_down);
      }
    });
  }


  private int getNumberPickerValue(int id) {
    NumberPicker view = (NumberPicker) model.act.findViewById(id);
    return view.getValue();
  }

  private int getSpinnerPosition(int id) {
    Spinner view = (Spinner) model.act.findViewById(id);
    int position = view.getSelectedItemPosition();
    return -1 == position ? 0 : position;
  }

  private void initNumberPicker(int id, int maxValue, int minValue) {
    NumberPicker view = (NumberPicker) model.act.findViewById(id);
    view.setMaxValue(maxValue);
    view.setMinValue(minValue);
  }

  private void initSpinner(int id, int idArray) {
    Spinner view = (Spinner) model.act.findViewById(id);
    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(model.act, idArray, android.R.layout.simple_spinner_item);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    view.setAdapter(adapter);
  }

  private boolean isSwitchChecked(int id) {
    Switch view = (Switch) model.act.findViewById(id);
    return view.isChecked();
  }

  private void setNumberPickerValue(int id, int value) {
    NumberPicker view = (NumberPicker) model.act.findViewById(id);
    view.setValue(value);
  }

  private void setSpinnerPosition(int id, int position) {
    Spinner view = (Spinner) model.act.findViewById(id);
    view.setSelection(position);
  }

  private void setSwitchChecked(boolean checked, int id) {
    Switch view = (Switch) model.act.findViewById(id);
    view.setChecked(checked);
  }
}
