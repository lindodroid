package com.lindosoft.android.oszilla;

import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.DecimalFormat;

public class OsziMainView {
  private DecimalFormat format = new DecimalFormat("0.000");
  private OsziModel model;

  public OsziMainView(OsziModel model) {
    this.model = model;
  }

  public void updateView() {
    model.act.runOnUiThread(new Runnable() {
      public void run() {
        MenuItem item = model.act.menu.findItem(R.id.item_connection);
        item.setIcon(model.connectionIcon);
        item.setTitle(model.connectionTitle);
        ImageView imageView = (ImageView) model.act.findViewById(R.id.image);
        int min = Math.min(imageView.getHeight(), imageView.getWidth());
        float range1 = model.configuration.ch1On
            ? min * model.calibration.getFactor(1, model.configuration.ch1Gain) / 10f : 0f;
        TextView textRange1 = (TextView) model.act.findViewById(R.id.text_range1);
        textRange1.setText(format.format(range1) + " V");
        float range2 = model.configuration.ch2On
            ? min * model.calibration.getFactor(2, model.configuration.ch2Gain) / 10f : 0f;
        TextView textRange2 = (TextView) model.act.findViewById(R.id.text_range2);
        textRange2.setText(format.format(range2) + " V");
        float range3 = model.configuration.ch3On
            ? min * model.calibration.getFactor(3, model.configuration.ch3Gain) / 10f : 0f;
        TextView textRange3 = (TextView) model.act.findViewById(R.id.text_range3);
        textRange3.setText(format.format(range3) + " V");
        float range4 = model.configuration.ch4On
            ? min * model.calibration.getFactor(4, model.configuration.ch4Gain) / 10f : 0f;
        TextView textRange4 = (TextView) model.act.findViewById(R.id.text_range4);
        textRange4.setText(format.format(range4) + " V");
      }
    });
  }
}
