package com.lindosoft.android.oszilla;

import com.lindosoft.android.common.*;

public class Measurement {
  public static int getChannelGain(byte[] telegram, int channel) {
    byte gain = telegram[6 + (channel - 1) / 2];
    return 1 == (channel % 2) ? (0x0f & gain) : (0x0f & (gain >>> 4));
  }

  public static int getCmdId(byte[] telegram) {
    return 0x0f & telegram[3];
  }

  public static int getTimeValue(byte[] telegram) {
    return 0x0f & telegram[4];
  }

  public static int getTriggerChannel(byte[] telegram) {
    return (0x03 & (telegram[9] >>> 4));
  }

  public static int getTriggerLevel(byte[] telegram) {
    int left = 0xff & telegram[9];
    int right = 0x0f & telegram[8];
    return (left << 8) | right;
  }

  public static boolean isChannelOn(byte[] telegram, int channel) {
    int mask = 1 << (channel - 1);
    return mask == (mask & telegram[5]);
  }

  public static boolean isTriggerAuto(byte[] telegram) {
    return 0x40 == (0x40 & telegram[9]);
  }

  public static boolean isTriggerDown(byte[] telegram) {
    return 0x80 == (0x80 & telegram[9]);
  }

  public static void accept(int size, byte[] telegram, int channel, SampleVisitor visitor) {
    if (!isChannelOn(telegram, channel)) {
      // Logger.info(Measurement.class, "skip channel " + channel);
      return;
    }
    int channels = 0;
    int offset = 0;
    int timeValue = getTimeValue(telegram);
    for (int i = 1; i < 5; i++) {
      if (isChannelOn(telegram, i)) {
        channels++;
        offset = i < channel ? 1 : offset;
      }
    }
    int delta = channels > 2 || timeValue > 5 ? 4 : channels;
    offset = channels > 2 ? channel - 1 : offset;
    int pos = 0;
    int posMax = timeValue > 2 ? 1000 / delta : timeValue * 200 / delta;
    float posDelta = 1000f / posMax; // from min 1 (1000/1000) to max 20 (1000/50) 
    // Logger.info(Measurement.class, "channel=" + channel + ", channels=" + channels + ", delta=" + delta + ", offset=" + offset);
    for (int i = 11 + 2 * offset; i < size && pos < posMax; i += 2 * delta) {
      int sample = (short) ((telegram[i] << 8) | (0xff & telegram[i-1]));
      visitor.visit(posDelta * pos++, sample);
    }
  }
}