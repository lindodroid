package com.lindosoft.android.oszilla;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerOpenListener;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import com.lindosoft.android.common.*;

/** Oszi activity. */
public class OsziAct extends AbstractAct {
  Menu menu;
  private OsziImageView imageView;
  private OsziMainView mainView;
  private OsziConfigurationView configurationView;
  private OsziModel model;

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.oszi, menu);
    this.menu = menu;
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case R.id.item_connection:
        return true;
      case R.id.item_rotation:
        rotateScreen();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void updateConfigurationView() {
    configurationView.updateView();
  }

  public void updateImageView() {
    imageView.updateView();
  }

  public void updateMainView() {
    mainView.updateView();
  }

  
  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.oszi);
    Swipe.registerSwipe(true, this, findViewById(R.id.image));
		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (bluetoothAdapter== null || !bluetoothAdapter.isEnabled()) {
      Intent settingsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
      startActivity(settingsIntent);
    }
    model = new OsziModel(this);
    configurationView = new OsziConfigurationView(model);
    imageView = new OsziImageView(model);
    mainView = new OsziMainView(model);
    model.doCreate(bundle);
    final ImageButton handle = (ImageButton) findViewById(R.id.handle);
    final SlidingDrawer drawer = (SlidingDrawer) findViewById(R.id.drawer);
    drawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {
      @Override
      public void onDrawerOpened() {
        handle.setImageResource(R.drawable.down);
        model.readConfiguration();
      }
    });         
    drawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
      @Override
      public void onDrawerClosed() {
        handle.setImageResource(R.drawable.up);
        configurationView.updateModel();
        model.writeConfiguration();
        updateMainView();
      }
    });
  }

  @Override
  protected void doDestroy() {
    model.doDestroy();
  }

  @Override
  protected void doPause() {
    model.doPause();
  }

  @Override
  protected void doResume() {
    model.doResume();
  }
}