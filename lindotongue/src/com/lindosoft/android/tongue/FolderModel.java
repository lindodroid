package com.lindosoft.android.tongue;

import android.os.Environment;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Shared folder data. */
public class FolderModel {
  public static final String HOME = "lindotongue";
  public static File folder = null;
  public static List<File> folders = new ArrayList<File>();

  static {
    Logger.init(getHome());
  }

  /** Add folder. */
  public static void addFolder(String name) {
    if (0 == name.trim().length()) {
      for (int i = 0; i < 10000; i++) {
        name = ("0000" + i).substring(("" + i).length());
        folder = new File(getHome(), name);
        if (!folder.exists()) {
          break;
        }
      }
    }
    folder = new File(getHome(), name);
    folder.mkdirs();
    readFolders();
  }
          
  /** Read folders. */
  public static void readFolders() {
    List<File> foldersTemp = new ArrayList<File>();
    for (File fileTemp : getHome().listFiles()) {
      if (fileTemp.isDirectory()) {
        foldersTemp.add(fileTemp);
      }
    }
    Collections.sort(foldersTemp);
    folders.clear();
    folders.addAll(foldersTemp);
  }

  /** Remove folder. */
  public static void removeFolder(File folderTemp) {
    folder = null;
    folders.remove(folderTemp);
    for (File fileTemp : folderTemp.listFiles()) {
      fileTemp.delete();
    }
    folderTemp.delete();
  }


  /** Get home directory. */
  private static File getHome() {
    File home = new File(Environment.getExternalStorageDirectory(), HOME);
    home.mkdirs();
    try {
      new File(home, ".nomedia").createNewFile();
    } catch (IOException ioEx) {
      Logger.warn(FolderModel.class, "create .nomedia file", ioEx);
    }
    return home;
  }
}