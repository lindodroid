package com.lindosoft.android.tongue;

import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;
import java.io.File;

/** Tongues base. */
public abstract class TonguesBase extends AbstractAct implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
  protected boolean first = true;
  protected int loop = 0;
  private Object lock = new Object();
  private long touch = 0L;

  public TonguesBase() {
    screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    stopVideo(true);
    startNext();
  }

  @Override
  public boolean onError(MediaPlayer mp, int what, int extra) {
    stopVideo(false);
    startNext();
    return true;
  }
  
  
  @Override
  protected void doBack() {
    stopNext();
    stopVideo(false);
    TongueModel.previousTongue();
    TongueModel.previousTongue();
    loop = MainModel.loop - 1;
    doFirst();
  }

  @Override
  protected void doCreate(Bundle bundle) {
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.tongues);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_tongues));
    doFirst();
  }

  @Override
  protected void doDestroy() {
    stopNext();
    stopVideo(false);
  }

  /** Override this. */
  protected abstract void doFirst();

  @Override
  protected void doForward() {
    stopNext();
    stopVideo(false);
    loop = MainModel.loop - 1;
    next();
  }

  /** Override this. */
  protected abstract void doSecond();

  @Override
  protected void doTap() {
    rotateScreen();
  }

  /** Next doFirst or doSecond. */
  protected void next() {
    if (first) {
      doFirst();
    } else {
      doSecond();
    }
  }

  /** Set drawing. */
  protected void setDrawing(File drawing) {
    ImageView imageView = (ImageView) findViewById(R.id.image_view);
    if (null != imageView) {
      if (null != drawing && drawing.isFile()) {
        imageView.setImageBitmap(new BitmapFactory().decodeFile(drawing.getAbsolutePath()));
      } else {
        imageView.setImageBitmap(null);
      }
    }
  }
      
  /** Set name. */
  protected void setName(String name) {
    int[] viewIds = {R.id.text_top, R.id.text_left, R.id.text_bottom, R.id.text_right};
    for (int viewId : viewIds) {
      TextView textView = (TextView) findViewById(viewId);
      if (null != textView) {
        textView.setText(null == name || 0 == name.length() ? "" : name);
      }
    }
  }

  /** Start next. */
  protected void startNext() {
    stopNext();
    if (-1L == MainModel.pause) {
      return;
    }
    final long touchTemp = touch;
    new Thread(new Runnable() {
      public void run() {
        synchronized(lock) {
          try {
            lock.wait(MainModel.pause);
          } catch (Exception ignore) {
          }
        }
        if (touchTemp == touch) {
          runOnUiThread(new Runnable() {
            public void run() {
              next();
            }
          });
        }
      }
    }).start();
  }

  /** Start video. */
  protected void startVideo(String video, boolean visibility) {
    stopNext();
    try {
      VideoView videoView = (VideoView) findViewById(R.id.video);
      videoView.setVisibility(View.INVISIBLE);
      String videoPath = new File(FolderModel.folder, video).getAbsolutePath();
      videoView.setOnCompletionListener(this);
      videoView.setOnErrorListener(this);
      videoView.setVideoPath(videoPath);
      View black = (View) findViewById(R.id.black);
      black.setAlpha(visibility ? 0.0f : 1.0f);
      videoView.setVisibility(View.VISIBLE);
      videoView.start();
    } catch (Exception ex) {
      Logger.warn(getClass(), "startVideo(" + video + ", " + visibility + ") failed", ex);
      stopVideo(false);
    }
  }

  /** Stop next. */
  protected void stopNext() {
    touch = System.currentTimeMillis();
    synchronized(lock) {
      lock.notifyAll();
    }
  }

  /** Stop video. */
  protected void stopVideo(boolean visibility) {
    try {
      VideoView videoView = (VideoView) findViewById(R.id.video);
      videoView.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
      videoView.stopPlayback();
    } catch (Exception ex) {
    }
  }
}