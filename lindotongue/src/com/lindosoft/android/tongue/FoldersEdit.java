package com.lindosoft.android.tongue;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.io.File;

/** Add/Remove folders. */
public class FoldersEdit extends AbstractFolders {
  /** Folder add clicked. */
  public void onFolderAdd(View view) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setIcon(R.drawable.folder_add);
    alert.setTitle(R.string.alert_title_folder_add);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View folderEdit = (View) inflater.inflate(R.layout.folder_edit, null);
    alert.setView(folderEdit);
    final DrawView folderDraw = (DrawView) folderEdit.findViewById(R.id.folder_draw);
    final EditText folderName = (EditText) folderEdit.findViewById(R.id.folder_name);
    alert.setPositiveButton(R.string.alert_ok_folder_add, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        String name = folderName.getText().toString();
        FolderModel.addFolder(name);
        writeDrawing(folderDraw, new File(FolderModel.folder, "folder.png"));
        Intent intent = new Intent(FoldersEdit.this, TonguesEdit.class);
        startActivity(intent);
      }
    });
    alert.show();
  }


  @Override
  protected int getContentViewId() {
    return R.layout.folders_edit;
  }
  
  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    File folder = adapter.getItem(position);
    FolderModel.folder = folder;
    Intent intent = new Intent(this, TonguesEdit.class);
    startActivity(intent);
  }
}