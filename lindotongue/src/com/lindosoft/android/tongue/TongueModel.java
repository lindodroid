package com.lindosoft.android.tongue;

import java.io.*;
import java.util.*;

/** Shared tongue data. */
public class TongueModel {
  public static final String FILENAME = "lindotongue.txt";
  public static final String REPLACE = "${SlAsH}";
  public static final String SEPARATOR = "/";
  public static Tongue tongue = null;
  public static List<Tongue> tongues = new ArrayList<Tongue>();

  /** Add tongue. */
  public static void addTongue() throws Exception {
    int idMax = 0;
    for (Tongue tongueTemp : tongues) {
      idMax = Math.max(idMax, tongueTemp.id);
    }
    tongue = new Tongue();
    tongue.id = idMax + 1;
    tongues.add(tongue);
    writeTongues();
  }
          
  /** Set next tongue. */
  public static void nextTongue() {
    if (0 == tongues.size()) {
      tongue = null;
      return;
    }
    if (null == tongue || -1 == tongues.indexOf(tongue) || tongues.size() - 1 == tongues.indexOf(tongue)) {
      tongue = tongues.get(0);
    } else {
      tongue = tongues.get(tongues.indexOf(tongue) + 1);
    }
  }

  /** Set previous tongue. */
  public static void previousTongue() {
    if (0 == tongues.size()) {
      tongue = null;
      return;
    }
    if (null == tongue || 1 > tongues.indexOf(tongue)) {
      tongue = tongues.get(tongues.size() - 1);
    } else {
      tongue = tongues.get(tongues.indexOf(tongue) - 1);
    }
  }

  /** Read tongues. */
  public static void readTongues() throws Exception {
    List<Tongue> tonguesTemp = new ArrayList<Tongue>();
    File file = new File(FolderModel.folder, FILENAME);
    if (!file.isFile()) {
      file.createNewFile();
    }
    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
    try {
      String line = null;
      while (null != (line = reader.readLine())) {
        tonguesTemp.add(readTongue(line));
      }
    } finally {
      reader.close();
      reader = null;
    }
    Collections.sort(tonguesTemp);
    tongues.clear();
    tongues.addAll(tonguesTemp);
  }

  /** Remove tongue. */
  public static void removeTongue(Tongue tongueTemp) throws Exception {
    tongue = null;
    tongues.remove(tongueTemp);
    new File(FolderModel.folder, tongueTemp.videoForeign).delete();
    new File(FolderModel.folder, tongueTemp.videoNative).delete();
    new File(FolderModel.folder, tongueTemp.getId() + "DrawingForeign.png").delete();
    new File(FolderModel.folder, tongueTemp.getId() + "DrawingNative.png").delete();
    writeTongues();
  }
  
  /** Reverse tongue. */
  public static void reverseTongue(Tongue tongueTemp) throws Exception {
    String nameForeign = tongueTemp.nameForeign;
    tongueTemp.nameForeign = tongueTemp.nameNative;
    tongueTemp.nameNative = nameForeign;
    File videoForeign = new File(FolderModel.folder, tongueTemp.videoForeign);
    File videoForeignTemp = new File(FolderModel.folder, tongueTemp.videoForeign + ".tmp");
    File videoNative = new File(FolderModel.folder, tongueTemp.videoNative);
    File drawingForeign = new File(FolderModel.folder, tongueTemp.getId() + "DrawingForeign.png");
    File drawingForeignTemp = new File(FolderModel.folder, tongueTemp.getId() + "DrawingForeign.png.tmp");
    File drawingNative = new File(FolderModel.folder, tongueTemp.getId() + "DrawingNative.png");
    if (videoForeign.isFile()) {
      videoForeign.renameTo(videoForeignTemp);
    }
    if (videoNative.isFile()) {
      videoNative.renameTo(videoForeign);
    }
    if (videoForeignTemp.isFile()) {
      videoForeignTemp.renameTo(videoNative);
    }
    if (drawingForeign.isFile()) {
      drawingForeign.renameTo(drawingForeignTemp);
    }
    if (drawingNative.isFile()) {
      drawingNative.renameTo(drawingForeign);
    }
    if (drawingForeignTemp.isFile()) {
      drawingForeignTemp.renameTo(drawingNative);
    }
    writeTongues();
  }     

  /** Shuffle tongues. */
  public static void shuffleTongues() {
    if (MainModel.shuffle) {
      Collections.shuffle(tongues, new Random(System.currentTimeMillis()));
    }
  }

  /** Write tongues. */
  public static void writeTongues() throws Exception {
    Collections.sort(tongues);
    File file = new File(FolderModel.folder, FILENAME);
    PrintWriter writer = new PrintWriter(file, "UTF-8");
    try {
      for (Tongue tongueTemp : tongues) {
        writeTongue(tongueTemp, writer);
      }
    } finally {
      writer.close();
      writer = null;
    }
  }


  /** Read tongue. */
  private static Tongue readTongue(String line) {
    String[] tokens = line.split(SEPARATOR, -1);
    Tongue tongueTemp = new Tongue();
    tongueTemp.id = Integer.parseInt(tokens[0]);
    tongueTemp.nameForeign = tokens[1].replace(REPLACE, SEPARATOR);
    tongueTemp.nameNative = tokens[2].replace(REPLACE, SEPARATOR);
    tongueTemp.videoForeign = tokens[3];
    tongueTemp.videoNative = tokens[4];
    return tongueTemp;
  }

  /** Write tongue. */
  private static void writeTongue(Tongue tongue, PrintWriter writer) {
    writer.println(tongue.id
        + SEPARATOR + tongue.nameForeign.replace(SEPARATOR, REPLACE)
        + SEPARATOR + tongue.nameNative.replace(SEPARATOR, REPLACE)
        + SEPARATOR + tongue.videoForeign
        + SEPARATOR + tongue.videoNative);
    writer.flush();
  }
}