package com.lindosoft.android.tongue;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

/** Show settings. */
public class Settings extends PreferenceActivity {
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    addPreferencesFromResource(R.xml.settings);
  }
}