package com.lindosoft.android.tongue;

import java.io.Serializable;

/** Tongue structure. */
public class Tongue implements Comparable, Serializable {
  public int id = 0;
  public String nameForeign = "";
  public String nameNative = "";
  public String videoForeign = "";
  public String videoNative = "";

  @Override
  public int compareTo(Object other) {
    try {
      return Double.valueOf(toString()).compareTo(Double.valueOf("" + other));
    } catch (RuntimeException rtEx) {
      return toString().compareTo("" + other);
    }
  }

  /** Get id as string. */
  public String getId() {
    return ("0000" + id).substring(("" + id).length());
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object other) {
    return toString().equals("" + other);
  }

  @Override
  public String toString() {
    return (0 == nameNative.trim().length()) ? getId() : nameNative;
  }
}