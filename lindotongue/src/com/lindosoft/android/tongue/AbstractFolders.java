package com.lindosoft.android.tongue;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.CancelDialog;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;

/** Show list of folders. */
public abstract class AbstractFolders extends AbstractAct implements OnMenuItemClickListener {
  @Override
  public boolean onMenuItemClick(MenuItem item) {
    final File folder = FolderModel.folder;
    switch (item.getItemId()) {
      case R.id.menu_folders_remove:
        new CancelDialog() {
          public void doRun() throws Exception {
            ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
            adapter.remove(folder);
            FolderModel.removeFolder(folder);
          }
        }.show(this, "removeFolder(" + folder + "): ", getResources().getString(R.string.prompt_cancel_remove), 2000L);
        return true;
      case R.id.menu_folders_rename:
        renameFolder(folder);
        return true;
      default:
        return false;
    }
  }


  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(getContentViewId());
    ArrayAdapter<File> adapter = new ArrayAdapter<File>(this, R.layout.folders_item, R.id.label) {
      public View getView(int position, View convertView, ViewGroup parent) {
        final File folder = FolderModel.folders.get(position);
        View view = super.getView(position, convertView, parent);
        ImageView drawing = (ImageView) view.findViewById(R.id.drawing);
        TextView label = (TextView) view.findViewById(R.id.label);
        File png = new File(folder, "folder.png");
        if (png.isFile()) {
          drawing.setImageDrawable(Drawable.createFromPath(png.getAbsolutePath()));
          drawing.setScaleType(ImageView.ScaleType.FIT_START);
          label.setPadding(6, 2, 0, 2);
          label.setText(null);
        } else {
          drawing.setImageDrawable(null);
          label.setText(folder.getName());
          label.setPadding(6, 2, 32, 2);
        }
        ImageView menu = (ImageView) view.findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
          public void onClick(View view) {
            FolderModel.folder = folder;
            showMenu(view);
          }
        });
        if (null != FolderModel.folder && position == getPosition(FolderModel.folder)) {
          view.setBackgroundColor(Color.DKGRAY);
        } else {
          view.setBackgroundColor(Color.BLACK);
        }
        return view;
      }
		};
    setListAdapter(adapter);
  }
  
  @Override
  protected void doResume() {
    FolderModel.readFolders();
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    adapter.clear();
    for (File file : FolderModel.folders) {
      adapter.add(file);
    }
  }

  /** Implement this to set the content view. */
  protected abstract int getContentViewId();

  protected void writeDrawing(DrawView drawView, File file) {
    if (null == drawView || !drawView.isDrawing()) {
      file.delete();
      return;
    }
    try {
      drawView.setDrawingCacheEnabled(true);
      Bitmap bitmap = drawView.getDrawingCache();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
      drawView.destroyDrawingCache();
    } catch (Exception ex) {
      Logger.warn(getClass(), "writeDrawing(drawView, " + file + ") failed", ex);
    }
  }


  /** Rename folder. */
  private void renameFolder(final File folder) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setIcon(R.drawable.folder);
    alert.setTitle(R.string.alert_title_folder_rename);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View folderEdit = (View) inflater.inflate(R.layout.folder_edit, null);
    final String nameOld = folder.getName();
    final DrawView folderDraw = (DrawView) folderEdit.findViewById(R.id.folder_draw);
    folderDraw.setDrawing(new File(folder, "folder.png"));
    final EditText folderName = (EditText) folderEdit.findViewById(R.id.folder_name);
    folderName.setText(nameOld);
    alert.setView(folderEdit);
    alert.setPositiveButton(R.string.alert_ok_folder_rename, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        String name = folderName.getText().toString();
        File folderNew = new File(folder.getParentFile(), name);
        if (0 == name.length()) {
          return;
        }
        if(name.equalsIgnoreCase(nameOld) || folder.renameTo(folderNew)) {
          FolderModel.folders.remove(folder);
          writeDrawing(folderDraw, new File(folderNew, "folder.png"));
          FolderModel.folders.add(folderNew);
          FolderModel.folder = folderNew;
          Collections.sort(FolderModel.folders);
          ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
          adapter.clear();
          for (File folder : FolderModel.folders) {
            adapter.add(folder);
          }
          scroll(adapter.getPosition(FolderModel.folder));
        }
      }
    });
    alert.show();
  }

  private void showMenu(View v) {
    PopupMenu popup = new PopupMenu(this, v);
    popup.setOnMenuItemClickListener(this);
    popup.inflate(R.menu.folders);
    popup.show();
  }
}