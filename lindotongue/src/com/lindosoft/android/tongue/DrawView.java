package com.lindosoft.android.tongue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.io.File;

public class DrawView extends View {
	private Bitmap canvasBitmap = null;
  private Paint canvasPaint = null;
	private Canvas drawCanvas = null;
	private Paint drawPaint = null;
  private long lastUp = 0L;
  private PointF point = null;

	public DrawView(Context context, AttributeSet attributes){
		super(context, attributes);
		drawPaint = new Paint();
		drawPaint.setARGB(255, 255, 192, 64);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(8.0f);
		drawPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		canvasPaint = new Paint(Paint.DITHER_FLAG);
	}

  public boolean isDrawing() {
    return null != canvasBitmap;
  }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
    if (null == drawCanvas) {
      canvasBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
      drawCanvas = new Canvas(canvasBitmap);
    }
		float touchX = event.getX();
		float touchY = event.getY();
    if (null != point) {
        drawCanvas.drawLine(point.x, point.y, touchX, touchY, drawPaint);
        invalidate();
    }
    drawCanvas.drawPoint(touchX, touchY, drawPaint);
    point = new PointF(touchX, touchY);
		if (MotionEvent.ACTION_UP == event.getAction()) {
      point = null;
      if (System.currentTimeMillis() - lastUp < 200L) {
        canvasBitmap = null;
        drawCanvas = null;
        invalidate();
      }
      lastUp = System.currentTimeMillis();
		}
		return true;
	}

  public void setDrawing(File drawing) {
    canvasBitmap = null;
    drawCanvas = null;
    if (null != drawing && drawing.isFile()) {
      canvasBitmap = new BitmapFactory().decodeFile(drawing.getAbsolutePath());
    }
  }


	@Override
	protected void onDraw(Canvas canvas) {
    if (null != canvasBitmap) {
      canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
    } else {
      canvas.drawRGB(0, 0, 0);
    }
	}
}