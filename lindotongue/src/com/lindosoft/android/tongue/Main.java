package com.lindosoft.android.tongue;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.lindosoft.android.common.AbstractAct;

public class Main extends AbstractAct {
  /** Folders edit clicked. */
  public void onFoldersEdit(View view) {
    Intent intent = new Intent(this, FoldersEdit.class);
    startActivity(intent);
  }

  /** Folders foreign clicked. */
  public void onFoldersForeign(View view) {
    Intent intent = new Intent(this, FoldersForeign.class);
    startActivity(intent);
  }

  /** Folders native clicked. */
  public void onFoldersNative(View view) {
    Intent intent = new Intent(this, FoldersNative.class);
    startActivity(intent);
  }

  /** Help clicked. */
  public void onHelp(View view) {
    String help = "http://android.lindosoft.com/?page_id=179";
    Uri uri = Uri.parse(help);
    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    startActivity(intent);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      }
    return super.onOptionsItemSelected(item);
  }

  /** Settings clicked. */
  public void onSettings(View view) {
    Intent intent = new Intent(this, Settings.class);
    startActivity(intent);
  }


  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.main);
  }

  @Override
  protected void doResume() {
    MainModel.setPreferences(PreferenceManager.getDefaultSharedPreferences(this));
  }
}
