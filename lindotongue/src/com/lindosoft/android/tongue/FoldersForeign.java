package com.lindosoft.android.tongue;

import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.File;

/** Foreign folders. */
public class FoldersForeign extends AbstractFolders {
  @Override
  protected int getContentViewId() {
    return R.layout.folders;
  }
  
  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    File folder = adapter.getItem(position);
    FolderModel.folder = folder;
    try {
      TongueModel.readTongues();
    } catch (Exception ex) {
      Toast.makeText(this, "readTongues(): " + ex, Toast.LENGTH_LONG).show();
    }
    if (0 == TongueModel.tongues.size()) {
      Toast.makeText(this, R.string.toast_folder_empty, Toast.LENGTH_LONG).show();
      return;
    }
    TongueModel.shuffleTongues();
    TongueModel.tongue = null;
    Intent intent = new Intent(this, TonguesForeign.class);
    startActivity(intent);
  }
}