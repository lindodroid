package com.lindosoft.android.tongue;

import java.io.File;

/** Native tongues. */
public class TonguesNative extends TonguesBase {
  @Override
  protected void doFirst() {
    loop++;
    if (loop == MainModel.loop) {
      TongueModel.nextTongue();
      loop = 0;
    }
    if (null == TongueModel.tongue) {
      next();
      return;
    }
    setName(TongueModel.tongue.nameNative);
    File drawing = new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingNative.png");
    setDrawing(drawing);
    first = false;
    if (0 != TongueModel.tongue.videoNative.length()) {
      startVideo(TongueModel.tongue.videoNative, true);
    } else if (0 != TongueModel.tongue.nameNative.length() || drawing.isFile()) {
      stopVideo(false);
      startNext();
    } else {
      next();
    }
  }

  @Override
  protected void doSecond() {
    first = true;
    setName(TongueModel.tongue.nameForeign);
    File drawing = new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingForeign.png");
    setDrawing(drawing);
    if (0 != TongueModel.tongue.videoForeign.length()) {
      startVideo(TongueModel.tongue.videoForeign, true);
    } else if (0 != TongueModel.tongue.nameForeign.length() || drawing.isFile()) {
      stopVideo(false);
      startNext();
    } else {
      next();
    }
  }
}