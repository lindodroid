package com.lindosoft.android.tongue;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.CancelDialog;
import java.io.File;

/** Add/Edit/Remove tongues. */
public class TonguesEdit extends AbstractAct implements OnMenuItemClickListener {
  @Override
  public boolean onMenuItemClick(MenuItem item) {
    final Tongue tongue = TongueModel.tongue;
    switch (item.getItemId()) {
      case R.id.menu_tongues_edit:
        Intent intent = new Intent(this, TongueEdit.class);
        startActivity(intent);
        return true;
      case R.id.menu_tongues_remove:
        new CancelDialog() {
          public void doRun() throws Exception {
            ArrayAdapter<Tongue> adapter = (ArrayAdapter<Tongue>) getListAdapter();
            adapter.remove(tongue);
            TongueModel.removeTongue(tongue);
          }
        }.show(this, "removeTongue(" + tongue + "): ", getResources().getString(R.string.prompt_cancel_remove), 1100L);
        return true;
      case R.id.menu_tongues_reverse:
        try {
          TongueModel.reverseTongue(tongue);
          onResume();
        } catch (Exception ex) {
          Toast.makeText(this, "reverseTongue(): " + ex, Toast.LENGTH_LONG).show();
        }
        return true;
      default:
        return false;
    }
  }

  /** Tongue add clicked. */
  public void onTongueAdd(View view) {
    try {
      TongueModel.addTongue();
      Intent intent = new Intent(this, TongueEdit.class);
      startActivity(intent);
    } catch (Exception ex) {
      Toast.makeText(this, "addTongue(): " + ex, Toast.LENGTH_LONG).show();
    }
  }

  
  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.tongues_edit);
    ArrayAdapter<Tongue> adapter = new ArrayAdapter<Tongue>(this, R.layout.tongues_item, R.id.label) {
      public View getView(int position, View convertView, ViewGroup parent) {
        final Tongue tongue = TongueModel.tongues.get(position);
        View view = super.getView(position, convertView, parent);
        ImageView drawing = (ImageView) view.findViewById(R.id.drawing);
        TextView label = (TextView) view.findViewById(R.id.label);
        File png = new File(FolderModel.folder, tongue.getId() + "DrawingNative.png");
        if (png.isFile()) {
          drawing.setImageDrawable(Drawable.createFromPath(png.getAbsolutePath()));
          drawing.setScaleType(ImageView.ScaleType.FIT_START);
          label.setPadding(6, 2, 0, 2);
          label.setText(null);
        } else {
          drawing.setImageDrawable(null);
          label.setText(TongueModel.tongues.get(position).toString());
          label.setPadding(6, 2, 32, 2);
        }
        ImageView menu = (ImageView) view.findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
          public void onClick(View view) {
            TongueModel.tongue = tongue;
            showMenu(view);
          }
        });
        if (null != TongueModel.tongue && position == getPosition(TongueModel.tongue)) {
          view.setBackgroundColor(Color.DKGRAY);
        } else {
          view.setBackgroundColor(Color.BLACK);
        }
        return view;
      }
		};
    setListAdapter(adapter);
  }

  @Override
  protected void doResume() {
    try {
      TongueModel.readTongues();
    } catch (Exception ex) {
      Toast.makeText(this, "readTongues(): " + ex, Toast.LENGTH_LONG).show();
    }
    ArrayAdapter<Tongue> adapter = (ArrayAdapter<Tongue>) getListAdapter();
    adapter.clear();
    for (Tongue tongue : TongueModel.tongues) {
      adapter.add(tongue);
    }
    scroll(adapter.getPosition(TongueModel.tongue));
  }

  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    ArrayAdapter<Tongue> adapter = (ArrayAdapter<Tongue>) getListAdapter();
    Tongue tongue = adapter.getItem(position);
    TongueModel.tongue = tongue;
    TongueModel.previousTongue();
    Intent intent = new Intent(this, TonguesNative.class);
    startActivity(intent);
  }
  

  private void showMenu(View v) {
    PopupMenu popup = new PopupMenu(this, v);
    popup.setOnMenuItemClickListener(this);
    popup.inflate(R.menu.tongues);
    popup.show();
  }
}