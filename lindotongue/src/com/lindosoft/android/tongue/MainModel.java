package com.lindosoft.android.tongue;

import android.content.SharedPreferences;

/** Main shared data. */
public class MainModel {
  public static int loop = 1;
  public static long pause = -1L;
  public static boolean shuffle = true;
  public static int swipeThreshold = 70;
  
  /** Read settings from shared preferences. */
  public static void setPreferences(SharedPreferences preferences) {
    loop = Integer.parseInt(preferences.getString("loop", "1"));
    pause = Long.parseLong(preferences.getString("pause", "-1"));
    shuffle = preferences.getBoolean("shuffle", false);
  }
}
