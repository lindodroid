package com.lindosoft.android.tongue;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.IoUtil;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.io.FileOutputStream;

/** Edit tongue. */
public class TongueEdit extends AbstractAct {
  public final int TONGUE_EDIT = 222;
  private boolean foreign = true;

  @Override
  public void onBackPressed() {
    onReady(null);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onReady(null);
        return true;
      }
    return super.onOptionsItemSelected(item);
  }

  /** Ready clicked. */
  public void onReady(View view) {
    if (foreign) {
      endForeign();
    } else {
      endNative();
      beginForeign();
    }
  }

  /** Video clicked. */
  public void onVideo(View view) {
    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
    startActivityForResult(intent, TONGUE_EDIT);
  }

  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (RESULT_OK != resultCode) {
      return;
    }
    String path = getPath(data.getData());
    String extension = path.substring(path.lastIndexOf("."));
    File from = new File(path);
    if (TONGUE_EDIT == requestCode) {
      try {
        if (foreign) {
          String video = TongueModel.tongue.getId() + "VideoForeign." + extension;
          File to = new File(FolderModel.folder, video);
          IoUtil.copyFile(from, to);
          TongueModel.tongue.videoForeign = video;
        } else {
          String video = TongueModel.tongue.getId() + "VideoNative." + extension;
          File to = new File(FolderModel.folder, video);
          IoUtil.copyFile(from, to);
          TongueModel.tongue.videoNative = video;
        }
        from.delete();
      } catch (Exception ex) {
        Logger.warn(getClass(), "onActivityResult() failed", ex);
      }
    }
  }

  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.tongue_edit);
    beginNative();
  }


  private void beginNative() {
    foreign = false;
    setTitle(R.string.title_edit_native);
    Button button = (Button) findViewById(R.id.button_continue);
    button.setText(R.string.button_continue);
    DrawView draw = (DrawView) findViewById(R.id.draw);
    draw.setDrawing(new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingNative.png"));
    EditText name = (EditText) findViewById(R.id.edit_name);
    name.setText(TongueModel.tongue.nameNative);
  }

  private void beginForeign() {
    foreign = true;
    setTitle(R.string.title_edit_foreign);
    Button button = (Button) findViewById(R.id.button_continue);
    button.setText(R.string.button_ready);
    DrawView draw = (DrawView) findViewById(R.id.draw);
    draw.setDrawing(new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingForeign.png"));
    EditText name = (EditText) findViewById(R.id.edit_name);
    name.setText(TongueModel.tongue.nameForeign);
  }

  private void endNative() {
    EditText name = (EditText) findViewById(R.id.edit_name);
    TongueModel.tongue.nameNative = name.getText().toString().trim();
    writeDrawing(new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingNative.png"));
    try {
      TongueModel.writeTongues();
    } catch (Exception ex) {
      Logger.warn(getClass(), "writeTongues() failed", ex);
    }
  }

  private void endForeign() {
    EditText name = (EditText) findViewById(R.id.edit_name);
    TongueModel.tongue.nameForeign = name.getText().toString().trim();
    writeDrawing(new File(FolderModel.folder, TongueModel.tongue.getId() + "DrawingForeign.png"));
    try {
      TongueModel.writeTongues();
    } catch (Exception ex) {
      Logger.warn(getClass(), "writeTongues() failed", ex);
    }
    finish();
  }

  /** Get path from video uri. */
  private String getPath(Uri uri) {
    String[] projection = { MediaStore.Video.Media.DATA };
    CursorLoader loader = new CursorLoader(this, uri, projection, null, null, null);
    Cursor cursor = loader.loadInBackground();
    startManagingCursor(cursor);
    int columnIndex = cursor.getColumnIndexOrThrow(projection[0]);
    cursor.moveToFirst();
    return cursor.getString(columnIndex);
  }
  
  private void writeDrawing(File file) {
    DrawView drawView = (DrawView) findViewById(R.id.draw);
    if (null == drawView || !drawView.isDrawing()) {
      file.delete();
      return;
    }
    try {
      drawView.setDrawingCacheEnabled(true);
      Bitmap bitmap = drawView.getDrawingCache();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
      drawView.destroyDrawingCache();
    } catch (Exception ex) {
      Logger.warn(getClass(), "writeDrawing(" + file + ") failed", ex);
    }
  }
}