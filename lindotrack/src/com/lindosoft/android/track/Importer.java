package com.lindosoft.android.track;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;
import java.io.*;
import java.util.*;

/** Show import view. */
public class Importer extends AbstractAct {
  private static boolean move = false;
  private static boolean overwrite = false;
  private String path = null;
  
  /** Cancel button clicked. */
  public void onCancel(View view) {
    onBack();
  }

  /** Import button clicked. */
  public void onImport(View view) {
    List<File> files = new ArrayList<File>();
    if (null != path && path.endsWith(TrackModel.EXTENSION)) {
      files.add(new File(path));
    }
    doImport(files);
  }
  
  /** ImportAll button clicked. */
  public void onImportAll(View view) {
    List<File> files = new ArrayList<File>();
    File parent = null == path ? null : new File(path).getParentFile();
    if (null != parent) {
      for (File child : parent.listFiles()) {
        if (child.getName().endsWith(TrackModel.EXTENSION)) {
          files.add(child);
        }
      }
    }
    doImport(files);
  }


  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.importer);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_importer));
    path = getIntent().getData().getPath();
    ((TextView) findViewById(R.id.text_path)).setText(path);
    ((CheckBox) findViewById(R.id.checkbox_move)).setChecked(move);
    ((CheckBox) findViewById(R.id.checkbox_overwrite)).setChecked(overwrite);
  }
  
  
  /** Copy (and split) source file to target directory. */
  private void copyFile(boolean overwrite, File source, File target) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(source));
    try {
      StringWriter head = null;
      String line = null;
      String name = source.getName();
      StringWriter writer = new StringWriter();
      PrintWriter printer = new PrintWriter(writer);
      while (null != (line = reader.readLine())) {
        if (-1 != line.indexOf("</trk>")) {
          // end track
          printer.println(line);
          printer.println("</gpx>");
          printer.flush();
          writeFile(head.toString(), writer.toString(), getTarget(target, name, overwrite));
          continue;
        } else if (-1 != line.indexOf("<trk>")) {
          if (null == head) {
            // end head
            printer.flush();
            head = writer;
          }
          // start track
          writer = new StringWriter();
          printer = new PrintWriter(writer);
        } else if  (-1 != line.indexOf("<name>")) {
          name = line.substring(line.indexOf("<name>") + 6, line.lastIndexOf("</name>"));          
        }
        printer.println(line);
      }
    } finally {
      reader.close();
    }
  }
    
  /** Import files to track directory. */
  private void doImport(List<File> files) {
    if (0 == files.size()) {
      Toast.makeText(this, R.string.toast_importer, Toast.LENGTH_LONG).show();
    }
    move = ((CheckBox) findViewById(R.id.checkbox_move)).isChecked();
    overwrite = ((CheckBox) findViewById(R.id.checkbox_overwrite)).isChecked();
    try {
      File base = TrackModel.getBase(this);
      for (File file : files) {
        copyFile(overwrite, file, base);
        if (move) {
          file.delete();
        }
      }
    } catch (Throwable ioEx) {
      Logger.warn(getClass(), "doImport()", ioEx);
    }
    onBack();
  }
  
  /** Get unique target file if !overwrite. */
  private File getTarget(File base, String name, boolean overwrite) {
    name = name.endsWith(TrackModel.EXTENSION) ? name : name + TrackModel.EXTENSION;
    File target = new File(base, name);
    if (!overwrite) {
      while (target.isFile()) {
        name = name.substring(0, name.lastIndexOf(".")) + "Z" + TrackModel.EXTENSION;
        target = new File(base, name);
      }
    }
    return target;
  }
  
  /** Write head and track to target. */
  private void writeFile(String head, String track, File target) throws IOException {
    FileWriter writer = new FileWriter(target);
    try {
      writer.write(head, 0, head.length());
      writer.write(track, 0, track.length());
      writer.flush();
    } finally {
      writer.close();
    }
    MainModel.fixFile(this, target);
  }    
}