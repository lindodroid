package com.lindosoft.android.track;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageView;
import com.lindosoft.android.common.Logger;

/** Start and stop location listener. */
public abstract class AbstractLocationList extends AbstractTrackList implements LocationListener {
  protected Location location = null;
  private LocationManager locationManager = null;
  private SyncPlayer player = null;

  @Override
  public void onBack() {
    stopLocation();
    super.onBack();
  }

  @Override
  public void onLocationChanged(Location location) {
    if (null == location || !location.hasAccuracy() || location.getAccuracy() > 50.0f) {
      return;
    }
    if (location.hasBearing() && location.hasSpeed()) {
      this.location = location;
      handleLocation();
    }
  }

  @Override
  public void onProviderDisabled(String provider) {
  }

  @Override
  public void onProviderEnabled(String provider) {
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
  }

  /** Handle the latest location change. */
  protected abstract void handleLocation();

  /** Play audio resource. */
  protected void play(String resource, boolean startNext) {
    if (!MainModel.phones) {
      return;
    }
    MediaPlayer mediaPlayer = null;
    try {
      int resourceId = getResources().getIdentifier(resource, "raw", getClass().getPackage().getName());
      mediaPlayer = MediaPlayer.create(this, resourceId);
    } catch (Throwable ex) {
      Logger.warn(getClass(), "play()", ex);
    }
    SyncPlayer next = new SyncPlayer((AudioManager) getSystemService(Context.AUDIO_SERVICE), mediaPlayer, startNext);
    player = player.setNext(next);
  }

  protected void startLocation() {
    try {
      player = new SyncPlayer((AudioManager) getSystemService(Context.AUDIO_SERVICE), null, true);
      location = null;
      locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
      boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
      if (!gpsEnabled) {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
      }
      locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000L, 0F, this);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "startLocation()", ignore); 
    }
  }

  protected void stopLocation() {
    locationManager.removeUpdates(this);
  }
}