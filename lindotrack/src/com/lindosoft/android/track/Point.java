package com.lindosoft.android.track;

import android.location.Location;
import java.io.File;

/** Gps point. */
public class Point implements Comparable {
  private static final String POSITION = "0000";
  public Location location = null;
  public int position = -1;
  public File track = null;

  @Override
  public int compareTo(Object other) {
    return toString().compareTo("" + other);
  }

  /** Get track-position. */
  public String getId() {
    return track + "-" + position;
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object other) {
    return (other instanceof Point) && ((Point) other).getId().equals(getId());
  }

  @Override
  public String toString() {
    return -1 == position ? "" : (POSITION + position).substring(("" + position).length());
  }
}