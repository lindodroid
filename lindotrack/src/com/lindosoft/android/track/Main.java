package com.lindosoft.android.track;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.view.View;
import android.widget.TextView;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;

/** Show main menu. */
public class Main extends AbstractAct {
  private static final int IMPORTER = 17;

  
  /** Importer clicked. */
  public void onImporter(View view) {
    TrackModel.mode = "tracks";
    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.addCategory(Intent.CATEGORY_OPENABLE);
    intent.addCategory(Intent.CATEGORY_DEFAULT);
    intent.setType("file/*");
    // android-11: intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
    intent.putExtra(Explorer.EXTRA_HOME, MainModel.getDownloads().getAbsolutePath());
    startActivityForResult(
        Intent.createChooser(intent, getResources().getString(R.string.title_chooser_importer)), IMPORTER);
  }

  /** Settings clicked. */
  public void onSettings(View view) {
    Intent intent = new Intent(this, Settings.class);
    startActivity(intent);
  }

  /** Tracklogs clicked. */
  public void onTracklogs(View view) {
    Intent intent = new Intent(this, Tracklogs.class);
    startActivity(intent);
  }

  /** Tracks clicked. */
  public void onTracks(View view) {
    Intent intent = new Intent(this, Tracks.class);
    startActivity(intent);
  }


  @Override
  protected void doBack() {
    notificationRemove();
  }
  
  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.main);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_main));
    Swipe.registerSwipe(false, this, findViewById(R.id.text_importer));
    Swipe.registerSwipe(false, this, findViewById(R.id.text_settings));
    Swipe.registerSwipe(false, this, findViewById(R.id.text_tracklogs));
    Swipe.registerSwipe(false, this, findViewById(R.id.text_tracks));
  }

  @Override
  protected void doResume() {
    MainModel.readSettings(PreferenceManager.getDefaultSharedPreferences(this));
    notificationAdd(R.string.content_text);
    TextView textSettings = (TextView) findViewById(R.id.text_settings);
    int activityId = getResources().getIdentifier("item_activity_" + MainModel.activity, "string", getClass().getPackage().getName());
    textSettings.setText(activityId);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    if (IMPORTER == requestCode && RESULT_OK == resultCode) {
      intent.setClass(this, Importer.class);
      startActivity(intent);
    }
  }
    

  /** Add/update notification. */
  private void notificationAdd(int contentTextId) {
    try {
      Intent intent = new Intent(this, this.getClass());
      intent.setAction(Intent.ACTION_MAIN);
      intent.addCategory(Intent.CATEGORY_LAUNCHER);
      Builder builder = new Builder(this)
          .setContentText(getString(contentTextId))
          .setContentTitle(getString(R.string.label))
          .setSmallIcon(R.drawable.icon_small);
      builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0));
      NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      Notification notification = builder.build();
      notificationManager.notify(1, notification);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "notificationAdd()", ignore);
    }
  }
  
  /** Remove notification. */
  private void notificationRemove() {
    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.cancelAll();
  }
}