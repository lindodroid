package com.lindosoft.android.track;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.util.Collections;

/** Show list of tracks. */
public class Tracks extends Tracklogs {
  @Override
  protected String getMode() {
    return "tracks";
  }

  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    showTrack((File) getListAdapter().getItem(position));
  }


  /** Show specified track. */
  private void showTrack(File track) {
    TrackModel.track = track;
    try {
      TrackModel.readTrackpoints();
      Intent intent = new Intent(this, TrackActivity.class);
      startActivity(intent);
    } catch (Throwable ex) {
      Logger.warn(getClass(), "showTrack()", ex);
    }
  }
}