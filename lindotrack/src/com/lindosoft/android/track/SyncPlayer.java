package com.lindosoft.android.track;

import android.media.AudioManager;
import android.media.MediaPlayer;
import com.lindosoft.android.common.Logger;

/** Synchronous media playback. */
public class SyncPlayer implements AudioManager.OnAudioFocusChangeListener, MediaPlayer.OnCompletionListener {
  private static int streamVolume = -1;
  private static int streamVolumeOther = -1;
  private AudioManager audioManager = null;
  private SyncPlayer next = null;
  private MediaPlayer player = null;
  private boolean startNext = true;

  /** Full constructor. */
  public SyncPlayer(AudioManager audioManager, MediaPlayer player, boolean startNext) {
    this.audioManager = audioManager;
    this.player = player;
    this.startNext = startNext;
  }

  @Override
  public void onAudioFocusChange(int focusChange) {
  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    try {
      streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
      if (-1 != streamVolumeOther) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolumeOther, 0);
      }
      if (null != player) {
        player.stop();
        player.release();
      }
    } catch (Throwable ex) {
      Logger.warn(getClass(), "onCompletion()", ex);
    }
    try {
      audioManager.abandonAudioFocus(this);
    } catch (Throwable ex) {
      Logger.warn(getClass(), "onCompletion().audioManager.abandonAudioFocus()", ex);
    }
    new Thread() {
      public void run() {
        try {
          Thread.sleep(666L);
          player = null;
          if (null != next) {
            next.start();
          }
        } catch (Throwable ex) {
          Logger.warn(getClass(), "onCompletion().thread.run()", ex);
        }
      }
    }.start();
  }

  /** Set next player. */
  public SyncPlayer setNext(SyncPlayer next) {
    try {
      if (!startNext && null != player) {
        if (null != next && null != next.player) {
          next.player.release();
          next.player = null;
        }
        return this; // accept next only if this is finished
      }
      this.next = next;
      if (null == player && null != next) {
        next.start();
      }
    } catch (Throwable ex) {
      Logger.warn(getClass(), "setNext()", ex);
    }
    return next;
  }

  /** Start player. */
  private void start() {
    if (null == player) {
      return;
    }
    try {
      player.setOnCompletionListener(this);
      player.setScreenOnWhilePlaying(true);
      streamVolumeOther = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
      if (-1 == streamVolume) {
        streamVolume = streamVolumeOther;
      }
      audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
      if (-1 != streamVolume) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, streamVolume, 0);
      }
      player.start();
    } catch (Throwable ex) {
      Logger.warn(getClass(), "start()", ex);
      try {
        if (null != player) {
          player.release();
        }
      } catch (Throwable ignore) {
        Logger.warn(getClass(), "start().player.release()", ex);
      }
      player = null;
    }
  }
}