package com.lindosoft.android.track;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import com.lindosoft.android.common.CancelDialog;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;
import java.io.File;
import java.util.Collections;

/** Show list of tracklogs. */
public class Tracklogs extends AbstractTrackList implements OnMenuItemClickListener {
  @Override
  public boolean onContextItemSelected(MenuItem item) {
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    int menuItemIndex = item.getItemId();
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    final File track = adapter.getItem(info.position);
    if (0 == menuItemIndex) {
      new CancelDialog() {
        public void doRun() throws Exception {
          removeTrack(track);
        }
      }.show(this, "removeTrack(" + track + "): ", getResources().getString(R.string.prompt_cancel_remove), 3000L);
    } else if (1 == menuItemIndex) {
      renameTrack(track);
    } else if (2 == menuItemIndex) {
			viewMap(track);
		}
    return true;
  }
    
  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    if (v != getListView()) {
      return;
    }
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    File track = adapter.getItem(info.position);
    TrackModel.track = track;
    String[] menuItems = getResources().getStringArray(R.array.tracks_context_menu);
    for (int i = 0; i<menuItems.length; i++) {
      menu.add(Menu.NONE, i, i, menuItems[i]);
    }
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    final File track = TrackModel.track;
    switch (item.getItemId()) {
      case R.id.menu_tracklogs_remove:
        new CancelDialog() {
          public void doRun() throws Exception {
            removeTrack(track);
          }
        }.show(this, "removeTrack(" + track + "): ", getResources().getString(R.string.prompt_cancel_remove), 3000L);
        return true;
      case R.id.menu_tracklogs_rename:
        renameTrack(track);
        return true;
      case R.id.menu_tracklogs_view_map:
        viewMap(track);
        return true;
      default:
        return false;
    }
  }

  /** Tracklog only. */
  public void onTracklog(View view) {
    TrackModel.startTracklog();
    Intent intent = new Intent(this, Tracklog.class);
    startActivity(intent);
  }


  @Override
  protected void doCreate(Bundle bundle) {
    if ("tracks".equals(getMode())) {
      setContentView(R.layout.tracks);
      Swipe.registerSwipe(true, this, findViewById(R.id.layout_tracks));
      Swipe.registerSwipe(false, this, getListView());
    } else {
      setContentView(R.layout.tracklogs);
      Swipe.registerSwipe(true, this, findViewById(R.id.layout_tracks));
      Swipe.registerSwipe(false, this, findViewById(R.id.text_tracklog));
      Swipe.registerSwipe(false, this, getListView());
    }
    ArrayAdapter<File> adapter = new ArrayAdapter<File>(this, R.layout.tracks_item, R.id.label) {
      public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView label = (TextView) view.findViewById(R.id.label);
        final File track = TrackModel.tracks.get(position);
        label.setText(track.getName());
        ImageView menu = (ImageView) view.findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
          public void onClick(View view) {
            TrackModel.track = track;
            showMenu(view);
          }
        });
        if (null != TrackModel.track && position == getPosition(TrackModel.track)) {
          view.setBackgroundColor(Color.DKGRAY);
        } else {
          view.setBackgroundColor(Color.BLACK);
        }
        return view;
      }
		};
    setListAdapter(adapter);
    registerForContextMenu(getListView());
  }

  @Override
  protected void doResume() {
    TrackModel.mode = getMode();
    TrackModel.readTracks(this);
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    adapter.clear();
    for (File track : TrackModel.tracks) {
      adapter.add(track);
    }
    scroll(adapter.getPosition(TrackModel.track));
  }

  protected String getMode() {
    return "tracklogs";
  }


  /** Remove track from tracks and from filesystem. */
  private void removeTrack(File track) {
    ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
    adapter.remove(track);
    TrackModel.tracks.remove(track);
    track.delete();
    MainModel.fixFile(this, track);
  }

  /** Rename track. */
  private void renameTrack(final File track) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setIcon(R.drawable.folder);
    alert.setTitle(R.string.alert_title_track_rename);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    final EditText editText = (EditText) inflater.inflate(R.layout.track_rename, null);
    final String nameTrack = track.getName().substring(0, track.getName().lastIndexOf("."));
    String data = "";
    try {
      GpxModel gpxModel = new GpxModel();
      gpxModel.readGpx(track);
      String hm = (long) (gpxModel.ascendLong * 0.83D) + "hm";
      String km = (long) (gpxModel.distanceLong / 1000.0F) + "km";
      data = "00000".substring(km.length()) + km + "-" + "000000".substring(hm.length()) + hm;
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "readGpx(" + track + ")", ignore);
    }
    String left = nameTrack.contains("-") ? nameTrack.substring(0, nameTrack.indexOf("-") + 1) : "";
    String right = nameTrack.contains("-") ? nameTrack.substring(nameTrack.indexOf("-") + 1) : nameTrack;
    final String nameOld = null != data && nameTrack.contains(data) ? nameTrack : left + data + "-" + right;
    editText.setText(nameOld);
    alert.setView(editText);
    final Context context = this;
    alert.setPositiveButton(R.string.alert_ok_track_rename, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int whichButton) {
        String name = editText.getText().toString();
        if (0 == name.length() || name.equalsIgnoreCase(nameOld)) {
          return;
        }
        File trackNew = new File(track.getParentFile(), name + TrackModel.EXTENSION);
        if(track.renameTo(trackNew)) {
          MainModel.fixFile(context, track);
          MainModel.fixFile(context, trackNew);
          TrackModel.tracks.remove(track);
          TrackModel.tracks.add(trackNew);
          TrackModel.track = trackNew;
          if ("tracks".equals(getMode())) {
            Collections.sort(TrackModel.tracks);
          } else {
            Collections.sort(TrackModel.tracks, Collections.reverseOrder());
          }
          ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
          adapter.clear();
          for (File track : TrackModel.tracks) {
            adapter.add(track);
          }
          scroll(adapter.getPosition(TrackModel.track));
        }
      }
    });
    alert.show();
  }

  private void showMenu(View v) {
    PopupMenu popup = new PopupMenu(this, v);
    popup.setOnMenuItemClickListener(this);
    popup.inflate(R.menu.tracklogs);
    popup.show();
  }
}