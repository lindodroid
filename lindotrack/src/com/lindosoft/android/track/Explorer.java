package com.lindosoft.android.track;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;
import java.io.*;
import java.util.*;

/** Show explorer style file selector. */
public class Explorer extends AbstractAct {
  public static String EXTRA_HOME = "com.lindosoft.android.track.Explorer.home";
  private static File homeExport = null;
  private static File homeImport = null;
  private boolean export = true;
  private List<File> files = new ArrayList<File>();
  private File parent = null;

  /** Parent folder clicked. */
  public void onParent(View view) {
    if (null == parent) {
      return;
    }
    if (export) {
      homeExport = parent;
    } else {
      homeImport = parent;
    }
    buildFiles();
  }

  
  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.explorer);
    export = null == getIntent().getStringExtra(EXTRA_HOME);
    if (null == homeExport) {
      homeExport = TrackModel.getBase("tracklogs", this);
    }
    if (!export && null == homeImport) {
      homeImport = new File(getIntent().getStringExtra(EXTRA_HOME));
    }
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_explorer));
    Swipe.registerSwipe(false, this, getListView());
    ArrayAdapter<File> adapter = new ArrayAdapter<File>(this, R.layout.tracks_item, R.id.label) {
      public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView label = (TextView) view.findViewById(R.id.label);
        File file = files.get(position);
        label.setText(file.getName());
        ImageView icon = (ImageView) view.findViewById(R.id.icon);
        icon.setImageResource(file.isDirectory() ? R.drawable.ex_folder : R.drawable.ex_file);
        return view;
      }
    };
    setListAdapter(adapter);
  }

  @Override
  protected void doResume() {
    buildFiles();
  }

  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    File file = (File) getListAdapter().getItem(position);
    if (file.isDirectory()) {
      if (export) {
        homeExport = file;
      } else {
        homeImport = file;
      }
      buildFiles();
    } else {
      Intent intent = getIntent();
      intent.setData(Uri.parse(file.getPath()));
      setResult(RESULT_OK, intent);
      finish();
    }
  }
    
  
  /** List folders and files in home. */
  private void buildFiles() {
    files.clear();
    File home = export ? homeExport : homeImport;
    if (null == home || !home.isDirectory()) {
      return;
    }
    try {
      ((TextView) findViewById(R.id.text_home)).setText(home.getName());
      parent = home.getParentFile();
      List<File> filesTemp = new ArrayList<File>();
      List<File> foldersTemp = new ArrayList<File>();
      for (File file : home.listFiles()) {
        if (file.isDirectory()) {
          foldersTemp.add(file);
        } else {
          filesTemp.add(file);
        }
      }
      if (export) {
        Collections.sort(filesTemp, Collections.reverseOrder());
      } else {
        Collections.sort(filesTemp);
      }
      Collections.sort(foldersTemp);
      files.addAll(foldersTemp);
      files.addAll(filesTemp);
      ArrayAdapter<File> adapter = (ArrayAdapter<File>) getListAdapter();
      adapter.clear();
      for (File file : files) {
        adapter.add(file);
      }
    } catch (Throwable ex) {
      Logger.warn(getClass(), "buildFiles()", ex);
    }
  }
}