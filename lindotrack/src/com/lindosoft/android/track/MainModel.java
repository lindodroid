package com.lindosoft.android.track;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaScannerConnection;
import android.os.Environment;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.util.*;

/** Main shared data. */
public class MainModel {
  public static final String HOME = "lindotrack";
  public static final String KEY = "setting_average";
  public static final String LOGS = "debug";
  public static String activity = "";
  public static float average = 25.0F;
  public static long intervalPlay = 50000L;
  public static boolean phones = true;
  public static float radiusPlay = 45.0F;
  public static List<String[]> settings = new ArrayList<String[]>();
  public static float speedMin = 1.6F;
  public static int swipeThreshold = 100;
  public static long segmentMillis = 10000L;
  public static boolean tracklog = true;

  static {
    settings = new ArrayList<String[]>();
    //                        {       activity, average, intervalPlay, radiusPlay, speedMin}
    settings.add(new String[] {        "hiker",     "3",      "80000",       "23",    "0.2"});
    settings.add(new String[] {       "walker",     "6",      "80000",       "26",    "0.4"});
    settings.add(new String[] {       "jogger",    "10",      "80000",       "30",    "0.6"});
    settings.add(new String[] {       "runner",    "15",      "80000",       "35",    "1.0"});
    settings.add(new String[] {      "inliner",    "18",      "50000",       "38",    "1.2"});
    settings.add(new String[] {"mountainbiker",    "20",      "50000",       "40",    "1.4"});
    settings.add(new String[] {      "crosser",    "23",      "50000",       "43",    "1.5"});
    settings.add(new String[] {    "tourbiker",    "25",      "50000",       "45",    "1.6"});
    settings.add(new String[] {    "roadbiker",    "30",      "50000",       "50",    "2.0"});
    settings.add(new String[] {      "scooter",    "40",      "30000",       "60",    "2.6"});
    settings.add(new String[] {   "motorbiker",    "50",      "30000",       "70",    "3.4"});
    readSettings(null);
  }

  /** Get downloads directory. */
  public static File getDownloads() {
    File downloads = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DOWNLOADS);
    downloads.mkdirs();
    return downloads;
  }

  public static void fixFile(Context context, File path) {
    if(null != path && null != context) {
      if (path.isFile()) {
        path.setExecutable(true);
        path.setReadable(true);
        path.setWritable(true);
      }
      MediaScannerConnection.scanFile(context, new String[] {path.toString(), path.getParent()}, null, null);
    }
  }

  /** Read settings from shared preferences. */
  public static void readSettings(SharedPreferences preferences) {
    String[] setting = settings.get(7);
    activity = null == preferences ? null : preferences.getString(KEY, setting[0]);
    for (String[] settingTemp : settings) {
      if (settingTemp[0].equals(activity)) {
        setting = settingTemp;
        break;
      }
    }
    activity = setting[0];
    average = Float.parseFloat(setting[1]);
    intervalPlay = Long.parseLong(setting[2]);
    radiusPlay = Float.parseFloat(setting[3]);
    speedMin = Float.parseFloat(setting[4]);
    File home = new File(Environment.getExternalStorageDirectory(), HOME);
    File logs = new File(home, LOGS);
    Logger.init(logs);
  }
}
