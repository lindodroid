package com.lindosoft.android.track;

import android.graphics.Bitmap;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

/** Draw Bitmap to ImageView. */
public class BitmapView {
  private WeakReference<ImageView> reference = null;

  public BitmapView(ImageView view) {
    super();
    this.reference = new WeakReference<ImageView>(view);
  }

  public int getHeight() {
    return null == reference || null == reference.get() ? 0 : reference.get().getHeight();
  }

  public int getWidth() {
    return null == reference || null == reference.get() ? 0 : reference.get().getWidth();
  }

  public void setBitmap(Bitmap bitmap) {
    if (reference != null && bitmap != null) {
      final ImageView imageView = reference.get();
      if (imageView != null) {
        imageView.setImageBitmap(bitmap);
        imageView.postInvalidate();
      }
    }
  }
}
