package com.lindosoft.android.track;

import android.content.Context;
import android.location.Location;
import com.lindosoft.android.common.Logger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/** Write gpx tracklog. */
public class TracklogModel {
  public static Context context = null;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
  private static String foot = null;
  private static long lastMillis = 0L;
  private static boolean segment = false;

  static {
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    StringWriter writer = new StringWriter();
    PrintWriter printer = new PrintWriter(writer);
    try {
      printer.println("    </trkseg>");
      printer.println("  </trk>");
      printer.println("</gpx>");
      printer.flush();
    } catch (Throwable ignore) {
      Logger.warn(TracklogModel.class, "static", ignore);
    } finally {
      printer.close();
    }
    foot = writer.toString();
  }

  public static synchronized void write(Location location, File tracklog) throws Exception {
    long millis = System.currentTimeMillis() - lastMillis;
    if (location.getSpeed() < MainModel.speedMin) {
      // no movement: new segment before next trackpoint
      segment = true;
      return;
    } else if (millis > MainModel.segmentMillis) {
      // movement after break: new segment before this trackpoint
      segment = true;
    }
    if (!tracklog.isFile()) {
      writeTracklog(tracklog);
    }
    RandomAccessFile randomAccess = new RandomAccessFile(tracklog, "rw");
    long filePointer = randomAccess.length() - foot.getBytes("UTF-8").length;
    try {
      randomAccess.seek(filePointer);
      if (segment) {
        writeSegment(randomAccess);
      }
      writeTrackpoint(location, randomAccess);
    } finally {
      randomAccess.close();
    }
    MainModel.fixFile(context, tracklog);
  }

  /** Write segment. */
  private static void writeSegment(RandomAccessFile randomAccess) throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    PrintWriter printer = new PrintWriter(out);
    try {
      printer.println("    </trkseg>");
      printer.println("    <trkseg>");
      printer.flush();
    } finally {
      printer.close();
      printer = null;
    }
    randomAccess.write(out.toByteArray());
    segment = false;
  }

  /** Write trackpoint. */
  private static void writeTrackpoint(Location location , RandomAccessFile randomAccess) throws Exception {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    PrintWriter printer = new PrintWriter(out);
    try {
      printer.println("      <trkpt lat=\"" + location.getLatitude() + "\" lon=\"" + location.getLongitude() + "\">");
      printer.println("        <ele>" + location.getAltitude() + "</ele>");
      printer.println("        <time>" + dateFormat.format(new Date()) + "</time>");
      printer.println("      </trkpt>");
      printer.flush();
    } finally {
      printer.close();
      printer = null;
    }
    randomAccess.write(out.toByteArray());
    randomAccess.write(foot.getBytes("UTF-8"));
    lastMillis = System.currentTimeMillis();
  }

  /** Write empty gpx tracklog. */
  private static void writeTracklog(File tracklog) throws Exception {
    PrintWriter printer = new PrintWriter(tracklog, "UTF-8");
    try {
      printer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
      printer.println("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"lindoTrack\" version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">");
      printer.println("  <metadata>");
      printer.println("    <name>" + tracklog.getName() + "</name>");
      printer.println("    <time>" + dateFormat.format(new Date()) + "</time>");
      printer.println("  </metadata>");
      printer.println("  <trk>");
      printer.println("    <name>" + tracklog.getName() + "</name>");
      printer.println("    <trkseg>");
      printer.write(foot);
      printer.flush();
    } finally {
      printer.close();
      printer = null;
    }
    segment = false;
  }
}
