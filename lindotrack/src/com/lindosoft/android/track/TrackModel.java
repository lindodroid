package com.lindosoft.android.track;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/** Shared data for track activities. */
public class TrackModel {
  public static final String DATE_FORMAT = "yyyy-MM-dd-";
  public static final String EXTENSION = ".gpx";
  public static String mode = "tracks";
  public static int target = 0;
  public static File track = null;
  public static List<File> tracks = new ArrayList<File>();
  public static List<Point> trackpoints = new ArrayList<Point>();

  /** Get audio direction, return 12 if not available. */
  public static int getAudioDirection(float bearingTo) {
    return ((((int) bearingTo + 360 + 345) / 30) % 12) + 1;
  }

  /** Get audio distance. */
  public static int getAudioDistance(float distanceTo) {
    int distance = (int) distanceTo;
    if (distance < 100) {
      distance = (distance / 10) * 10;
    } else if (distance < 1000) {
      distance = (distance / 100) * 100;
    } else if (distance < 10000) {
      distance = (distance / 1000) * 1000;
    } else {
      distance = 10000;
    }
    return distance;
  }

  /** Get base directory. */
  public static File getBase(Context context) {
    return getBase("tracks".equals(mode) ? "tracks" : "tracklogs", context);
  }

  /** Get base directory. */
  public static File getBase(String basename, Context context) {
    File home = new File(Environment.getExternalStorageDirectory(), MainModel.HOME);
    File base = new File(home, basename);
    if (!base.exists()) {
      base.mkdirs();
      MainModel.fixFile(context, base);
    }
    return base;
  }

  /** Get bearing to target in degrees, 0 if location has no bearing. */
  public static float getBearingToTarget(Location location) {
    return location.hasBearing() ? location.bearingTo(trackpoints.get(target).location) - location.getBearing() : 0f;
  }

  /** Get distance to target. */
  public static float getDistanceToTarget(Location location) {
    return location.distanceTo(trackpoints.get(target).location);
  }

  /** Get track file belonging to current tracklog. */
  public static File getTrack() {
    String name = track.getName();
    name = name.length() > DATE_FORMAT.length() ? name.substring(DATE_FORMAT.length()) : "";
    for (String[] setting : MainModel.settings) {
      if (name.startsWith(setting[0] + "-")) {
        name = name.substring(setting[0].length() + 1);
        break;
      }
    }
    return new File(getBase("tracks", null), name);
  }

  /** Get tracklog file belonging to current track. */
  public static File getTracklog() {
    String name = track.getName();
    name = name.lastIndexOf(".") > 0 ? "-" + name.substring(0, name.lastIndexOf(".")) : "";
    name = new SimpleDateFormat("yyyy-MM-dd-").format(new Date()) + MainModel.activity + name + EXTENSION;
    return new File(getBase("tracklogs", null), name);
  }

  /** Read trackpoints of current track from filesystem. */
  public static void readTrackpoints() throws Exception {
    GpxModel gpxModel = new GpxModel();
    gpxModel.readGpx(track);
    trackpoints.clear();
    trackpoints.addAll(gpxModel.trackpoints);
    target = 0;
  }
      
  /** Read all tracks from the filesystem. */
  public static void readTracks(Context context) {
    tracks.clear();
    List<File> tracksTemp = new ArrayList<File>();
    File[] children = getBase(context).listFiles();
    for (File child : children) {
      if (child.getName().endsWith(EXTENSION)) {
        tracksTemp.add(child);
      }
    }
    if ("tracks".equals(mode)) {
      Collections.sort(tracksTemp);
    } else {
      Collections.sort(tracksTemp, Collections.reverseOrder());
    }
    tracks.clear();
    tracks.addAll(tracksTemp);
  }

  /** Set target nearest to location. */
  public static float setNearestTarget(Location location) {
    if (null == location || 0 == trackpoints.size()) {
      target = 0;
      return 0f;
    }
    float distanceNearest = location.distanceTo(trackpoints.get(0).location);
    int positionNearest = 0;
    for (int position = 1; position < trackpoints.size(); position++) {
      float distance = location.distanceTo(trackpoints.get(position).location);
      if (distance < distanceNearest) {
        distanceNearest = distance;
        positionNearest = position;
      }
    }
    target = positionNearest;
    return distanceNearest;
  }

  /** Tracklog only. */
  public static void startTracklog() {
    target = 0;
    track = new File(getBase(null), "tracklog");
    track = getTracklog();
    trackpoints.clear();
  }
}