package com.lindosoft.android.track;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import com.lindosoft.android.common.Logger;
import java.io.File;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/** Draw track based on bearing, location and zoom to ImageView. */
public class SpeedModel {
  public Location locationNew = null;
  public BitmapView view = null;
  public Bitmap speedometer = null;
  private float distanceLong = 0.0f;
  private float distanceShort = 0.0f;
  private int mode = 0;
  private long timeLong = 0L;
  private long timeShort = 0L;
  private Location locationOld = null;
  private float speed = 0.0f;
  private AsyncTask<Void, Void, Bitmap> task = null;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("HH'h'mm'm'ss's'");

  public void click() {
    mode = (mode + 1) % 3;
    if (1 == mode) {
      // reset short
      distanceShort = 0.0f;
      timeShort = 0L;
    }
  }

  public void readState(File file) {
    try {
      GpxModel gpxModel = new GpxModel();
      gpxModel.readGpx(file);
      distanceLong = gpxModel.distanceLong;
      timeLong = gpxModel.timeLong;
    } catch (Throwable ignore) {
      // Logger.warn(getClass(), "readState(" + file + ")", ignore);
    }
  }     

  public void readState(Bundle bundle) {
    if (null != bundle) {
      distanceLong = bundle.getFloat("speedModel.distanceLong");
      distanceShort = bundle.getFloat("speedModel.distanceShort");
      mode = bundle.getInt("speedModel.mode");
      timeLong = bundle.getLong("speedModel.timeLong");
      timeShort = bundle.getLong("speedModel.timeShort");
    }
  }

  public void update() {
    updateModel();
    if (null != task) {
      task.cancel(true);
      task = null;
    }
    if (null == view) {
      return;
    }
    task = new AsyncTask<Void, Void, Bitmap>() {
      @Override
      protected Bitmap doInBackground(Void... args) {
        return getBitmap();
      }

      @Override
      protected void onPostExecute(Bitmap bitmap) {
        if (null != view) {
          view.setBitmap(bitmap);
        }
      }
    };
    task.execute();
  }

  public void updateModel() {
    locationOld = null == locationOld ? locationNew : locationOld;
    if (null != locationNew) {
      if (locationNew.getSpeed() > MainModel.speedMin && locationOld.getSpeed() > MainModel.speedMin) {
        float distance = locationNew.distanceTo(locationOld);
        long time = locationNew.getTime() - locationOld.getTime();
        distanceLong += distance;
        timeLong += time;
        if (1 == mode) {
          distanceShort += distance;
          timeShort += time;
        }
      }
      speed = locationNew.getSpeed() * 3.6f;
    } else {
      speed = 0.0f;
    }
    locationOld = locationNew;
  }

  public void writeState(Bundle bundle) {
    if (null != bundle) {
      bundle.putFloat("speedModel.distanceLong", distanceLong);
      bundle.putFloat("speedModel.distanceShort", distanceShort);
      bundle.putInt("speedModel.mode", mode);
      bundle.putLong("speedModel.timeLong", timeLong);
      bundle.putLong("speedModel.timeShort", timeShort);
    }
  }

  private Bitmap getBitmap() {
    try {
      Thread.sleep(220L);
    } catch (Throwable ignore) {
      return null;
    }
    try {
      int height = null == view ? 0 : view.getHeight();
      int width = null == view ? 0 : view.getWidth();
      if (0 == height || 0 == width) {
        return null;
      }
      return getBitmap(height, width);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "getBitmap()", ignore);
      return null;
    }
  }

  public Bitmap getBitmap(int height, int width) {
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    float distance = 0 == mode ? distanceLong : distanceShort;
    long time = 0 == mode ? timeLong : timeShort;
    float average = 0 == time ? 0.0f : distance * 3600.0f / ((float) time);
    String text = 0 == mode ? "TOTAL" : 1 == mode ? "TIMER ON" : "TIMER OFF";
    String textAverage = new MessageFormat("{0,number,0.00}km/h").format(new Object[] {average});
    String textDistance = new MessageFormat("{0,number,0.00}km").format(new Object[] {distance / 1000.0f});
    String textTime = dateFormat.format(new Date(time));
    float speedMax = MainModel.average < 20.0f ? speedMax = 1.75f * MainModel.average : 2.75f * MainModel.average;
    int speedUnit = speedMax < 20.0f ? 1 : speedMax < 50.0f ? 5 : 10;
    int limit = ((int) speedMax) / speedUnit;
    int center = 200;
    Bitmap bitmap = Bitmap.createBitmap(2 * center, 2 * center, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    canvas.drawRGB(0, 0, 0);
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    if (null != speedometer) {
      canvas.drawBitmap(speedometer, new Rect(0, 0, speedometer.getWidth(), speedometer.getHeight()),
          new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), paint);
    }
    paint.setStrokeWidth(4);
    if (0 == mode) {
      paint.setARGB(255, 255, 192, 64);
    } else if (1 == mode) {
      paint.setARGB(255, 64, 224, 64);
    } else if (2 == mode) {
      paint.setARGB(255, 0, 192, 255);
    }
    paint.setTextAlign(Paint.Align.CENTER);
    paint.setTextSize(32);
    paint.setTypeface(Typeface.create(Typeface.DEFAULT_BOLD, Typeface.ITALIC));
    for (int i = 0; i < limit + 1; i++) {
      float angle = 64f + ((float) i) * 232f / ((float) limit);
      PointF p1 = getPoint(angle, center, 0.9f * center);
      PointF p2 = getPoint(angle, center, 0.8f * center);
      PointF p3 = getPoint(angle, center, 0.65f * center);
      canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
      centerText(p3.x, p3.y, "" + i * speedUnit, canvas, paint);
    }
    for (int i = 0; i < limit * 5; i++) {
      if (0 != i % 5) {
        float angle = 64f + ((float) i) * 46.4f / ((float) limit);
        PointF p1 = getPoint(angle, center, 0.9f * center);
        PointF p2 = getPoint(angle, center, 0.85f * center);
        canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
      }
    }
    paint.setTextSize(24);
    canvas.drawText(text, center, 120, paint);
    paint.setTextSize(40);
    canvas.drawText(textAverage, center, 160, paint);
    canvas.drawText(textDistance, center, center + 70, paint);
    paint.setTextSize(56);
    canvas.drawText(textTime, center, center + 120, paint);
    // average
    float angle = 64f + 232f * average / (float) (limit * speedUnit);
    PointF p1 = getPoint(angle, center, 0.7f * center);
    PointF p2 = getPoint(angle, center, 0.09f * center);
    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
    // speed
    paint.setStrokeWidth(6);
    paint.setARGB(255, 255, 0, 0);
    angle = 64f + 232f * speed / (float) (limit * speedUnit);
    p1 = getPoint(angle, center, 0.8f * center);
    p2 = getPoint(angle, center, 0.09f * center);
    canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
    return bitmap;
  }

  private void centerText(float x, float y, String text, Canvas canvas, Paint paint) {
    Rect bounds = new Rect();
    paint.getTextBounds(text, 0, text.length(), bounds);
    canvas.drawText(text, x, y - (float) bounds.centerY(), paint);
  }

  private PointF getPoint(float angle, int center, float radius) {
    double radians = Math.PI * angle / 180d;
    float cos = (float) Math.cos(radians);
    float sin = (float) Math.sin(radians);
    return new PointF((float) center - radius * sin, (float) center + radius * cos);
  }
}