package com.lindosoft.android.track;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.lindosoft.android.common.AbstractAct;
import com.lindosoft.android.common.Swipe;

/** Show settings. */
public class Settings extends AbstractAct {
  @Override
  protected void doCreate(Bundle bundle) {
    setContentView(R.layout.settings);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_settings));
    ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.settings_item, R.id.label) {
      public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView label = (TextView) view.findViewById(R.id.label);
        String activity = MainModel.settings.get(position)[0];
        int activityId = getResources().getIdentifier("item_activity_" + activity, "string", getClass().getPackage().getName());
        label.setText(activityId);
        if (MainModel.activity.equals("" + getItem(position))) {
          view.setBackgroundColor(Color.DKGRAY);
        } else {
          view.setBackgroundColor(Color.BLACK);
        }
        return view;
      }
		};
    setListAdapter(adapter);
    for (String[] setting : MainModel.settings) {
      adapter.add(setting[0]);
    }
  }
  
  @Override
  protected void onListItemClick(ListView l, View v, int position, long id) {
    String activity = MainModel.settings.get(position)[0];
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString(MainModel.KEY, activity);
    editor.commit();
    finish();
  }  
}