package com.lindosoft.android.track;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.lindosoft.android.common.Logger;
import java.util.List;

/** Draw track based on bearing, location and zoom to ImageView. */
public class MapModel {
  private static final int scope = 50;
  public SpeedModel speedModel = new SpeedModel();
  private Double altitudeMax = null;
  private Double altitudeMin = null;  
  private Location location = null;
  private int perspective = 0;
  private AsyncTask<Void, Void, Bitmap> task = null;
  private List<Point> track = null;
  private BitmapView view = null;
  private float zoom = 8192f;

  public void perspective() {
    perspective = (perspective + 1) % 4;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public void setTrack(List<Point> track) {
    this.track = track;
    altitudeMax = null;
    altitudeMin = null;
    for (Point point : track) {
      double altitude = point.location.getAltitude();
      altitudeMax = null == altitudeMax || altitudeMax < altitude ? altitude : altitudeMax;
      altitudeMin = null == altitudeMin || altitudeMin > altitude ? altitude : altitudeMin;
    }
  }

  public void setView(BitmapView view) {
    this.view = view;
  }

  public void zoomMinus() {
    zoom = zoom * 2f;
  }

  public void zoomPlus() {
    zoom = zoom / 2f;
  }

  public void update() {
    speedModel.locationNew = location;
    speedModel.updateModel();
    if (null != task) {
      task.cancel(true);
      task = null;
    }
    if (null == location || null == track || null == view) {
      return;
    }
    task = new AsyncTask<Void, Void, Bitmap>() {
      @Override
      protected Bitmap doInBackground(Void... args) {
        return getBitmap();
      }

      @Override
      protected void onPostExecute(Bitmap bitmap) {
        if (null != view) {
          view.setBitmap(bitmap);
        }
      }
    };
    task.execute();
  }


  private Bitmap getBitmap() {
    try {
      Thread.sleep(300L);
    } catch (Throwable ignore) {
      return null;
    }
    try {
      int height = null == view ? 0 : view.getHeight();
      int width = null == view ? 0 : view.getWidth();
      if (0 == height || 0 == width) {
        return null;
      }
      Bitmap map = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(map);
		  canvas.drawRGB(0, 0, 0);
      Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
      paint.setStrokeCap(Paint.Cap.ROUND);
      paint.setStrokeJoin(Paint.Join.ROUND);
      Bitmap speedometer = speedModel.getBitmap(height, width);
      Rect source = new Rect(0, 0, speedometer.getWidth(), speedometer.getHeight());
      Rect target = new Rect(Math.max(width - height, 0), 0, width, Math.min(height, width));
      canvas.drawBitmap(speedometer, source, target, paint);
      PointF origin = new PointF(width > height ? width / 4.0f : width / 2.0f, width > height ? height * 2f / 3f : height * 3.0f / 4.0f);
      drawScope(canvas, paint, origin);
      Point pointMin = drawTrack(canvas, paint, origin);
      drawAltitude(canvas, paint, pointMin, origin);
      return map;
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "getBitmap()", ignore);
      return null;
    }
  }

  private void drawAltitude(Canvas canvas, Paint paint, Point pointMin, PointF origin) {
    int height = view.getHeight();
    int width = view.getWidth();
    float scaleX = (float) width / (float) track.size();
    float scaleY = (float) height / (float) (altitudeMax - altitudeMin) / 3.0f;
    paint.setStrokeWidth(8);
    paint.setARGB(176, 255, 255, 255);
    Path path = new Path();
    PointF previousPointF = null;
    for (Point point : track) {
      PointF pointF = new PointF((float) point.position * scaleX, height - (float) (point.location.getAltitude() - altitudeMin) * scaleY);
      if (null != previousPointF) {
        path.moveTo(previousPointF.x, previousPointF.y);
        path.lineTo(pointF.x, pointF.y);
      }
      previousPointF = pointF;
    }
    canvas.drawPath(path, paint);
    float x = (float) pointMin.position * scaleX;
    float y = height - (float) (pointMin.location.getAltitude() - altitudeMin) * scaleY;
    paint.setStrokeWidth(10);
    paint.setARGB(255, 255, 0, 0);
    canvas.drawCircle(x, y, 16, paint);
  }

  private void drawScope(Canvas canvas, Paint paint, PointF origin) {
    int height = view.getHeight();
    int width = view.getWidth();
    paint.setARGB(224, 255, 255, 255);
    paint.setStrokeWidth(5);
    paint.setStyle(Paint.Style.STROKE);
    canvas.drawLine(origin.x, origin.y, origin.x, origin.y + scope, paint);
    canvas.drawLine(origin.x - scope, origin.y, origin.x + scope, origin.y, paint);
    int i1000 = (int) (zoom / 1000f);
    int i100 = (int) (zoom / 100f);
    int i10 = (int) (zoom / 10f);
    paint.setStrokeWidth(2);
    paint.setARGB(224, 255, 192, 64);
    for (int i = 1; i < i10 + 1 && i < 6 && i100 < 3; i++) {
      float r = ((float) i) * 10.0f;
      PointF radius = transform((float) Math.PI / 4f, r, origin);
      if (null != radius) {
        canvas.drawOval(new RectF(2f * origin.x - radius.x, radius.y, radius.x, 2f * origin.y - radius.y), paint);
//        canvas.drawCircle(origin.x, origin.y, radius.x - origin.x, paint);
      }
    }
    paint.setARGB(224, 64, 224, 64);
    for (int i = 1; i < i100 + 1 && i < 6 && i1000 < 3; i++) {
      float r = ((float) i) * 100.0f;
      PointF radius = transform((float) Math.PI / 4f, r, origin);
      if (null != radius) {
        canvas.drawOval(new RectF(2f * origin.x - radius.x, radius.y, radius.x, 2f * origin.y - radius.y), paint);
//        canvas.drawCircle(origin.x, origin.y, radius.x - origin.x, paint);
      }
    }
    paint.setARGB(224, 0, 192, 255);
    for (int i = 1; i < i1000 + 1 && i < 6; i++) {
      float r = ((float) i) * 1000.0f;
      PointF radius = transform((float) Math.PI / 4f, r, origin);
      if (null != radius) {
        canvas.drawOval(new RectF(2f * origin.x - radius.x, radius.y, radius.x, 2f * origin.y - radius.y), paint);
//        canvas.drawCircle(origin.x, origin.y, radius.x - origin.x, paint);
      }
    }
  }

  private Point drawTrack(Canvas canvas, Paint paint, PointF origin) {
    paint.setStrokeWidth(10);
    float distanceMin = 0.0f;
    Point pointMin = null;
    PointF previousPointF = null;
    int size = track.size();
    for (int i = size - 1; i >= 0; i--) {
      if (i > size / 2) {
        paint.setARGB(255, 128 - 128 * i / size, 255 - 64 * i / size, 384 * i / size - 129);
      } else {
        paint.setARGB(255, 255 - 384 * i / size, 192 + 64 * i / size, 64);
      }
      Point point = track.get(i);
      double bearingTo = (location.bearingTo(point.location) - location.getBearing()) * Math.PI / 180.0d;
      float distanceTo = location.distanceTo(point.location);
      distanceMin = null == pointMin || distanceTo <= distanceMin ? distanceTo : distanceMin;
      pointMin = distanceTo <= distanceMin ? point : pointMin;
      PointF pointF = transform((float) bearingTo, distanceTo, origin);
      if (null != previousPointF && null != pointF) {
        canvas.drawLine(previousPointF.x, previousPointF.y, pointF.x, pointF.y, paint);
      }
      previousPointF = pointF;
    }
    return pointMin;
  }

  private PointF transform(float bearingTo, float distanceTo, PointF origin) {
    switch(perspective) {
		case 0:
			return transform0(bearingTo, distanceTo, origin);
		case 1:
			return transform1(bearingTo, distanceTo, origin);
		case 2:
			return transform2(bearingTo, distanceTo, origin);
		case 3:
			return transform3(bearingTo, distanceTo, origin);
		default:
			return null;
	}
  }

  private PointF transform0(float bearingTo, float distanceTo, PointF origin) {
    float scale = origin.y / zoom;
    float x = (float) Math.sin(bearingTo) * distanceTo * scale + origin.x;
    float y = origin.y - (float) Math.cos(bearingTo) * distanceTo * scale;
    return new PointF(x, y);
  }

  private PointF transform1(float bearingTo, float distanceTo, PointF origin) {
    if (distanceTo > zoom) {
      distanceTo = zoom;
    }
    float r = (float) Math.sin((Math.PI * distanceTo) / (2f * zoom)) * origin.y;
    float x = (float) Math.sin(bearingTo) * r + origin.x;
    float y = origin.y - (float) Math.cos(bearingTo) * r;
    return new PointF(x, y);
  }

  private PointF transform2(float bearingTo, float distanceTo, PointF origin) {
    float r = (float) Math.atan((4f * distanceTo) / zoom) * 2f * origin.y / (float) Math.PI;
    float x = (float) Math.sin(bearingTo) * r + origin.x;
    float y = origin.y - (float) Math.cos(bearingTo) * r;
    return new PointF(x, y);
  }

  private PointF transform3(float bearingTo, float distanceTo, PointF origin) {
	double angle45 = Math.PI / 4d;
	double sin45 = Math.sin(angle45);
    float x = (float) Math.sin(bearingTo) * distanceTo * origin.y / zoom / (float) (1d - sin45) + origin.x;
    float y = (float) Math.cos(bearingTo) * distanceTo;
    if (y > zoom) {
      y = zoom;
    }
    if (y < -zoom) {
      y = -zoom;
    }
    y = origin.y - (float) ((Math.sin(angle45 * y / zoom + angle45)) - sin45) / (float) (1d - sin45) * origin.y;
    return new PointF(x, y);
  }
}