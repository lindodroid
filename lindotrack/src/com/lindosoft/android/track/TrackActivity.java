package com.lindosoft.android.track;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;
import java.io.File;
import java.util.Collections;

/** Display track in BitmapView. */
public class TrackActivity extends AbstractLocationList {
  private float distanceToGo = 0f;
  private float distanceToTargetMin = 100000f;
  private Location lastLocation = null;
  private MapModel model = null;
  private long timeout = 0L;
  private int zone = 3;

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.track_activity, menu);
    MenuItem item = menu.findItem(R.id.menu_item_toggle_phones);
    if (null != item) {
      item.setIcon(MainModel.phones ? R.drawable.phones_on : R.drawable.phones_off);
      item.setTitle(MainModel.phones ? R.string.menu_item_phones_off : R.string.menu_item_phones_on);
    }
    return true;
  }

  /** Get nearest clicked. */
  public void onGetNearest() {
    TrackModel.setNearestTarget(location);
    timeout = 0L;        
  }

  /** Minus clicked. */
  public void onMinus(View view) {
    model.zoomMinus();
    model.update();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case R.id.menu_track_get_nearest:
        onGetNearest();
        return true;
      case R.id.menu_track_reverse:
        onReverse();
        return true;
      case R.id.menu_track_view_map:
        viewMap(TrackModel.track);
        return true;
      case R.id.menu_item_perspective:
        perspective();
        return true;
      case R.id.menu_item_rotate_screen:
        rotateScreen();
        model.update();
        return true;
      case R.id.menu_item_toggle_phones:
        MainModel.phones = !MainModel.phones;
        item.setIcon(MainModel.phones ? R.drawable.phones_on : R.drawable.phones_off);
        item.setTitle(MainModel.phones ? R.string.menu_item_phones_off : R.string.menu_item_phones_on);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  /** Plus clicked. */
  public void onPlus(View view) {
    model.zoomPlus();
    model.update();
  }

  /** Reverse clicked. */
  public void onReverse() {
    Collections.reverse(TrackModel.trackpoints);
    TrackModel.target = 0;
    model.update();
  }

  public void perspective() {
    model.perspective();
    model.update();
  }

  
  @Override
  protected void doCreate(Bundle bundle) throws Exception {
    setContentView(R.layout.map);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_map));
    model = new MapModel();
    model.speedModel.speedometer = BitmapFactory.decodeResource(getResources(), R.drawable.speedometer);
    if (null != bundle) {
      TrackModel.mode = bundle.getString("mode");
      TrackModel.track = (File) bundle.getSerializable("track");
      int nextPosition = bundle.getInt("nextPosition");
      MainModel.readSettings(PreferenceManager.getDefaultSharedPreferences(this));
      TrackModel.readTrackpoints();
      if (TrackModel.trackpoints.size() > nextPosition) {
        TrackModel.target = nextPosition;
      }
      model.speedModel.readState(bundle);
    } else {
      model.speedModel.readState(TrackModel.getTracklog());
    }
    model.setTrack(TrackModel.trackpoints);
    if (TrackModel.trackpoints.size() > 0) {
      model.setLocation(TrackModel.trackpoints.get(0).location);
    }
    startLocation();
  }

  @Override
  protected void doDestroy() {
    stopLocation();
  }

  @Override
  protected void doPause() {
    model.setView(null);
  }

  @Override
  protected void doResume() {
    ImageView imageView = (ImageView) findViewById(R.id.image_map);
    BitmapView view = new BitmapView(imageView);
    model.setView(view);
    model.update();
    distanceToTargetMin = 100000f;
    zone = 3;
  }

  @Override
  protected void doSave(Bundle bundle) {
    bundle.putString("mode", TrackModel.mode);
    bundle.putSerializable("track", TrackModel.track);
    bundle.putInt("nextPosition", TrackModel.target);
    model.speedModel.writeState(bundle);
  }

  @Override
  protected void doTap() {
    model.speedModel.click();
    model.update();
  }

  @Override
  protected void handleLocation() {
    try {
      model.setLocation(location);
      model.update();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "handleLocation()", ignore);
    }
    try {
      if (MainModel.tracklog) {
        TracklogModel.context = this;
        TracklogModel.write(location, TrackModel.getTracklog());
      }
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "handleLocation().tracklogModel.write()", ignore);	
    }
    if (null == location || location.getSpeed() < MainModel.speedMin || 0 == TrackModel.trackpoints.size()) {
      return;
    }
    try {
      handleLocation(location);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "handleLocation()", ignore);	
    }
  }

  /** Handle location for audio instructions. */
  private void handleLocation(Location location) {
    if (null != lastLocation) {
      distanceToGo -= lastLocation.distanceTo(location);
    }
    lastLocation = location;
    float distanceToTarget = TrackModel.getDistanceToTarget(location);
    if (distanceToTarget < distanceToTargetMin) {
      distanceToTargetMin = distanceToTarget;
    } else if (distanceToTarget > 4f * distanceToTargetMin) {
      // target lost
      distanceToTarget = TrackModel.setNearestTarget(location);
      distanceToTargetMin = distanceToTarget;
    }
    if (distanceToTarget < MainModel.radiusPlay) {
      // target reached
      handleZoneOne(distanceToTarget, location);
    } else if (distanceToTarget < MainModel.radiusPlay * 3f) {
      // close to target
      handleZoneTwo(distanceToTarget, location);
    } else {
      // far away from target
      handleZoneThree(distanceToTarget, location);
    }
  }

  private void handleZoneOne(float distanceToTarget, Location location) {
    if (distanceToGo > MainModel.radiusPlay / 2f) {
      return;
    }
    if (TrackModel.target >= TrackModel.trackpoints.size() - 1) {
      // end of track reached
      if (zone > 1 || System.currentTimeMillis() > timeout) {
        play("stop", true);
        timeout = System.currentTimeMillis() + MainModel.intervalPlay / 3L;
        zone = 1;
      }
    } else if (location.hasBearing()) {
      // set new target
      distanceToGo = 0f;
      distanceToTargetMin = 100000L;
      int directionMax = 12;
      int positionMax = TrackModel.target;
      float bearing = 0 == TrackModel.target ? location.getBearing()
            : TrackModel.trackpoints.get(TrackModel.target - 1).location.bearingTo(TrackModel.trackpoints.get(TrackModel.target).location);
      for (int i = TrackModel.target + 1; i < TrackModel.trackpoints.size(); i++) {
        float bearingTo = TrackModel.trackpoints.get(i - 1).location.bearingTo(TrackModel.trackpoints.get(i).location);
        int direction = TrackModel.getAudioDirection(bearingTo - bearing);
        if (Math.abs(direction - 6) > Math.abs(directionMax - 6) && Math.abs(directionMax - 6) < 5) {
          break; // max direction already passed
        }
        distanceToGo += TrackModel.trackpoints.get(i - 1).location.distanceTo(TrackModel.trackpoints.get(i).location);
        directionMax = direction;
        positionMax = i;
        if (distanceToGo > MainModel.radiusPlay) {
          break; // too far away
        }
      }
      TrackModel.target = positionMax;
      if (directionMax < 12) {
        play("b" + directionMax, true);
      }
      timeout = System.currentTimeMillis() + MainModel.intervalPlay / 3L;
      zone = 1;
    } 
  }

  private void handleZoneTwo(float distanceToTarget, Location location) {
    if (zone > 2 || System.currentTimeMillis() > timeout) {
      int direction = TrackModel.getAudioDirection(TrackModel.getBearingToTarget(location));
      if (direction < 12) {
        play("b" + direction, true);
      }
      timeout = System.currentTimeMillis() + MainModel.intervalPlay / 3L;
      zone = 2;
    }
  }

  private void handleZoneThree(float distanceToTarget, Location location) {
    if (System.currentTimeMillis() > timeout) {
      int direction = TrackModel.getAudioDirection(TrackModel.getBearingToTarget(location));
      if (direction < 12) {
        play("b" + direction, true);
      }
      int distance = TrackModel.getAudioDistance(distanceToTarget);
      play("d" + distance, true);
      timeout = System.currentTimeMillis() + MainModel.intervalPlay;
      zone = 3;
    }
  }
}