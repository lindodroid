package com.lindosoft.android.track;

import android.location.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/** Read and write gpx files. */
public class GpxModel extends DefaultHandler {
  public double ascendLong = 0.0D;
  public float distanceLong = 0.0F;
  public long timeLong = 0L;
  public List<Point> trackpoints = new ArrayList<Point>();
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
  private double altitude = 0.0D;
  private StringBuilder builder = new StringBuilder();
  private Location locationOld = null;
  private Point point = null;
  private long time = 0L;

  private File track = null;

  static {
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  /** Parse xml from *.gpx file. */
  public void readGpx(File track) throws Exception {
    this.track = track;
    ascendLong = 0.0D;
    distanceLong = 0.0F;
    timeLong = 0L;
    locationOld = null;
    SAXParserFactory parserFactory = SAXParserFactory.newInstance();
    SAXParser parser = parserFactory.newSAXParser();
    XMLReader reader = parser.getXMLReader();
    // does not work: XMLReader reader = XMLReaderFactory.createXMLReader();
    reader.setContentHandler(this);
    reader.parse(new InputSource(new FileInputStream(track)));
  }

  @Override
  public void characters(char[] ch, int start, int length) {
    builder.append(ch, start, length);
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) {
    if ("ele".equals(localName)) {
      altitude = 0.0d;
      builder = new StringBuilder();
    } else if ("time".equals(localName)) {
      builder = new StringBuilder();
      time = 0L;
    } else if ("rpt".equals(localName) || "rtept".equals(localName) || "trkpt".equals(localName)) {
      point = new Point();
      Location location = new Location(LocationManager.GPS_PROVIDER);
      location.setLatitude(Double.parseDouble(attributes.getValue("lat")));
      location.setLongitude(Double.parseDouble(attributes.getValue("lon")));
      point.location = location;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) {
    if ("ele".equals(localName)) {
      altitude = Double.parseDouble(builder.toString());
    } else if ("time".equals(localName)) {
      try {
        time = dateFormat.parse(builder.toString()).getTime();
      } catch (Exception ignore) {
      }
    } else if ("rpt".equals(localName) || "rtept".equals(localName) || "trkpt".equals(localName)) {
      point.location.setAltitude(altitude);
      point.location.setTime(time);
      point.position = trackpoints.size();
      point.track = track;
      trackpoints.add(point);
      Location locationNew = point.location;
      locationOld = null == locationOld ? locationNew : locationOld;
      ascendLong += locationNew.hasAltitude() && locationOld.hasAltitude() && locationNew.getAltitude() > locationOld.getAltitude()
          ? locationNew.getAltitude() - locationOld.getAltitude() : 0.0D;
      distanceLong += locationNew.distanceTo(locationOld);
      timeLong += locationNew.getTime() - locationOld.getTime();
      locationOld = locationNew;
    } else if ("trkseg".equals(localName)) {
      locationOld = null;
    }
  }
}