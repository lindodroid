package com.lindosoft.android.track;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import com.lindosoft.android.common.AbstractAct;
import java.io.File;

/** Common base of track list activities. */
public abstract class AbstractTrackList extends AbstractAct {
	/** View track on map in different program. */
	protected void viewMap(final File track) {
    TrackModel.track = track;
    try {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.setDataAndType(Uri.fromFile(track), "application/gpx+xml");
      startActivity(intent);
    } catch (Throwable ex) {
      mytracksConfirm();
    }
  }  

  
  private void mytracksConfirm() {
    new AlertDialog.Builder(this)
        .setMessage(R.string.dialog_mytracks_message)
        .setPositiveButton(R.string.dialog_mytracks_positive, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
              mytracksInstall();
            }
        })
        .setNegativeButton(R.string.dialog_mytracks_negative, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        })
        .show();
  }
  
  private void mytracksInstall() {
    try {
      Thread.sleep(500L);
      Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.maps.mytracks"));
      startActivity(intent);
    } catch (Throwable ex) {
      Toast.makeText(this, R.string.toast_mytracks, Toast.LENGTH_LONG).show();
    }
  }
}