package com.lindosoft.android.track;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.lindosoft.android.common.Logger;
import com.lindosoft.android.common.Swipe;

/** Write tracklog. */
public class Tracklog extends AbstractLocationList {
  private SpeedModel speedModel = new SpeedModel();

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.tracklog, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case R.id.menu_item_rotate_screen:
        rotateScreen();
        speedModel.update();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

 
  @Override
  protected void doCreate(Bundle state) {
    setContentView(R.layout.tracklog);
    Swipe.registerSwipe(true, this, findViewById(R.id.layout_tracklog));
    speedModel.speedometer = BitmapFactory.decodeResource(getResources(), R.drawable.speedometer);
    if (null != state) {
      speedModel.readState(state);
      TrackModel.mode = state.getString("mode");
      MainModel.readSettings(PreferenceManager.getDefaultSharedPreferences(this));
      TrackModel.startTracklog();
    } else {
      speedModel.readState(TrackModel.track);
    }
    startLocation();
  }

  @Override
  protected void doDestroy() {
    stopLocation();
  }

  @Override
  protected void doPause() {
    speedModel.view = null;
  }

  @Override
  protected void doResume() {
    ImageView imageView = (ImageView) findViewById(R.id.image_tracklog);
    BitmapView bitmapView = new BitmapView(imageView);
    speedModel.view = bitmapView;
    speedModel.update();
  }

  @Override
  protected void doSave(Bundle state) {
    speedModel.writeState(state);
    state.putString("mode", TrackModel.mode);
  }

  @Override
  protected void doTap() {
    speedModel.click();
    speedModel.update();
  }
  
  @Override
  protected void handleLocation() {
    try {
      speedModel.locationNew = location;
      speedModel.update();
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "handleLocation()", ignore);
    }
    try {
      TracklogModel.context = this;
      TracklogModel.write(location, TrackModel.track);
    } catch (Throwable ignore) {
      Logger.warn(getClass(), "handleLocation()", ignore);
    }
  }
}